package com.infinitemind.przypominajka.interfaces;

public interface DataRunnable<T> {
	void run(T data);
}
