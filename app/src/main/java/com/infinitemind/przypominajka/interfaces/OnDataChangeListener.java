package com.infinitemind.przypominajka.interfaces;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public class OnDataChangeListener implements ValueEventListener {

	private DataRunnable<DataSnapshot> callback;

	public OnDataChangeListener(DataRunnable<DataSnapshot> callback) {
		this.callback = callback;
	}

	@Override
	public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
		callback.run(dataSnapshot);
	}

	@Override
	public void onCancelled(@NonNull DatabaseError databaseError) {
		Log.d("LOG!", "error: " + databaseError.getMessage());
	}
}
