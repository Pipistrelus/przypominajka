package com.infinitemind.przypominajka.interfaces;

import android.text.Editable;
import android.text.TextWatcher;

public class OnTextChangedListener implements TextWatcher {

	private DataRunnable<Editable> onTextChanged;

	public OnTextChangedListener(DataRunnable<Editable> onTextChanged) {
		this.onTextChanged = onTextChanged;
	}

	@Override
	public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	}

	@Override
	public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

	}

	@Override
	public void afterTextChanged(Editable editable) {
		onTextChanged.run(editable);
	}
}
