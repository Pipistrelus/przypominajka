package com.infinitemind.przypominajka.interfaces;

import android.animation.Animator;

public class OnAnimationEndListener implements Animator.AnimatorListener {

	private DataRunnable<Animator> onAnimationEnd;

	public OnAnimationEndListener(DataRunnable<Animator> onAnimationEnd) {
		this.onAnimationEnd = onAnimationEnd;
	}

	@Override
	public void onAnimationStart(Animator animator) {

	}

	@Override
	public void onAnimationEnd(Animator animator) {
		onAnimationEnd.run(animator);
	}

	@Override
	public void onAnimationCancel(Animator animator) {

	}

	@Override
	public void onAnimationRepeat(Animator animator) {

	}
}
