package com.infinitemind.przypominajka.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.LocationsListAdapter;
import com.infinitemind.przypominajka.utils.Constants;
import com.infinitemind.przypominajka.utils.Utils;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class LocationAddonFragment extends Fragment implements AddonFragment, OnMapReadyCallback {
	private FusedLocationProviderClient mFusedLocationClient;
	private OnLocationSetListener onLocationSetListener;
	private ArrayList<String> locations;
	private ViewGroup rootView;
	private GoogleMap googleMap;
	private MapView mapView;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_addon_location, container, false);

			mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
			mapView = rootView.findViewById(R.id.map);
			mapView.onCreate(savedInstanceState);
			mapView.onResume();
			MapsInitializer.initialize(getActivity().getApplicationContext());
			mapView.getMapAsync(this);

			setLocationsList();

		}
		if(!Utils.checkLocationServicesPermission(getContext())) {
			LocationRequest mLocationRequest = LocationRequest.create()
					.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
					.setInterval(10 * 1000)
					.setFastestInterval(1000);
			LocationSettingsRequest.Builder settingsBuilder = new LocationSettingsRequest.Builder()
					.addLocationRequest(mLocationRequest);
			settingsBuilder.setAlwaysShow(true);
			Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getContext())
					.checkLocationSettings(settingsBuilder.build());
			result.addOnCompleteListener(task -> {
				try {
					task.getResult(ApiException.class);
					locateUser();
				} catch(ApiException ex) {
					switch (ex.getStatusCode()) {
						case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
							try {
								startIntentSenderForResult(((ResolvableApiException) ex).getResolution().getIntentSender(), Constants.LOCATION_REQUEST_CODE, null, 0, 0, 0, null);
							} catch (IntentSender.SendIntentException ignored) { }
							break;
						case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
							break;
					}
				}
			});
		}
		return rootView;
	}

	private void setLocationsList() {
		locations = new ArrayList<>();
		locations.add("Rzeszów");
		locations.add("Przemyśl");

		RecyclerView locationsList = rootView.findViewById(R.id.locationsList);
		locationsList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
		locationsList.setAdapter(new LocationsListAdapter(locations));
	}

	@SuppressLint("MissingPermission")
	private void locateUser() {
		googleMap.setMyLocationEnabled(true);
		mFusedLocationClient.getLastLocation().addOnSuccessListener(location -> {
			if(location != null) googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
		});
	}

	@Override
	public int getIcon() {
		return R.drawable.ic_location;
	}

	public LocationAddonFragment setOnLocationSetListener(OnLocationSetListener onLocationSetListener) {
		this.onLocationSetListener = onLocationSetListener;
		return this;
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		this.googleMap = googleMap;

		if(Utils.checkLocationServicesPermission(getContext())) locateUser();
		else ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.ACCESS_COARSE_LOCATION}, Constants.LOCATION_REQUEST_CODE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Constants.LOCATION_REQUEST_CODE) {
			if(resultCode == RESULT_OK) locateUser();
			else showLocationDisabledDialog();
		}
	}

	private void showLocationDisabledDialog() {
		Utils.makeDialog(getActivity(), R.layout.dialog_confirmation, dialog -> {
			((AppCompatTextView) dialog.findViewById(R.id.title)).setText(getContext().getResources().getString(R.string.warning));
			((AppCompatTextView) dialog.findViewById(R.id.text)).setText(getContext().getResources().getString(R.string.location_warning));
			((AppCompatTextView) ((CardView) dialog.findViewById(R.id.yesButton)).getChildAt(0)).setText(getContext().getResources().getString(R.string.ok));
			dialog.findViewById(R.id.noButton).setVisibility(View.GONE);

			dialog.findViewById(R.id.yesButton).setOnClickListener(view -> {
				onLocationSetListener.onLocationDisabled();
				dialog.dismiss();
			});

			dialog.setOnDismissListener(view -> onLocationSetListener.onLocationDisabled());
			dialog.setOnCancelListener(view -> onLocationSetListener.onLocationDisabled());
		});
	}

	public interface OnLocationSetListener {
		void onLocationSet();
		void onLocationDisabled();
	}
}
