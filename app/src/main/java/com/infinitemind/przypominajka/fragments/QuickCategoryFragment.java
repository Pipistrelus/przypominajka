package com.infinitemind.przypominajka.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.CategoriesListAdapter;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Selectable;
import com.infinitemind.przypominajka.notifications.Notifications;

import java.util.ArrayList;

public class QuickCategoryFragment extends Fragment {

	private ViewGroup rootView;
	private CategoriesListAdapter categoriesListAdapter;
	private ArrayList<Category> categories;
	private OnQuickCategoryChangeListener onQuickCategoryChangeListener;
	private ArrayList<Category> quickCategories;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_selectable_list, container, false);

			setCategoriesList();
		}

		return rootView;
	}

	private void setCategoriesList() {
		RecyclerView categoriesList = rootView.findViewById(R.id.selectableList);
		categoriesList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
		categoriesList.setAdapter(categoriesListAdapter = new CategoriesListAdapter(categories = new ArrayList<>(Data.allCategories)));
		categoriesListAdapter.setOnItemClickListener(new CategoriesListAdapter.MyClickListener() {
			@Override public void onItemClick(int position) {
				boolean selected = categories.get(position).isSelected();
				if(Selectable.getSelectedAmount(categories) < 3 && !selected)
					categories.get(position).setSelected(true);
				else if(selected) categories.get(position).setSelected(false);
				new Handler().post(() -> categoriesListAdapter.notifyDataSetChanged());
				onQuickCategoryChangeListener.onQuickCategoryChange(categories);
			}
			@Override public void onItemLongClick(int position) { }
		});

		Selectable.setSelected(quickCategories, categories);
		categoriesListAdapter.notifyDataSetChanged();
	}

	public QuickCategoryFragment setOnQuickCategoryChangeListener(OnQuickCategoryChangeListener onQuickCategoryChangeListener) {
		this.onQuickCategoryChangeListener = onQuickCategoryChangeListener;
		return this;
	}

	public QuickCategoryFragment setQuickCategories(ArrayList<Category> quickCategories) {
		this.quickCategories = quickCategories;
		return this;
	}

	public interface OnQuickCategoryChangeListener {
		void onQuickCategoryChange(ArrayList<Category> categories);
	}
}