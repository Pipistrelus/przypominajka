package com.infinitemind.przypominajka.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.interfaces.DataRunnable;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Ringtone;
import com.infinitemind.przypominajka.models.Selectable;
import com.infinitemind.przypominajka.notifications.Notifications;
import com.infinitemind.przypominajka.settings.FragmentItem;
import com.infinitemind.przypominajka.settings.ImageItem;
import com.infinitemind.przypominajka.settings.SettingsItem;
import com.infinitemind.przypominajka.settings.SwitchItem;
import com.infinitemind.przypominajka.utils.Utils;
import com.infinitemind.przypominajka.views.ErrorMessageView;

import java.util.ArrayList;

public class SettingsListFragment extends Fragment implements RingtoneListFragment.OnRingtonePickListener {

	private GoogleSignInAccount lastLoggedAccount;
	private OnChangeListener onChangeListener;
	private ViewGroup rootView;
	public SettingsItem ringtoneItem;
	public SettingsItem vibrationItem;
	public SettingsItem quickAddItem;
	public SettingsItem quickCategoryItem;
	public SettingsItem synchronizeItem;
	public SettingsItem signInItem;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_settings_list, container, false);

			setSettingsList();
		}

		return rootView;
	}

	private void setSettingsList() {
		ViewGroup settingsList = rootView.findViewById(R.id.settingItemContainer);

		ringtoneItem = new FragmentItem(getContext())
				.setOnFragmentChangeListener(this::onChangeFragment)
				.setFragment(new RingtoneListFragment().setRingtoneUri(Data.selectedRingtone.getUri()).setOnRingtonePickListener(this))
				.setTitle(getResources().getString(R.string.ringtone)).setSubtitle(Data.selectedRingtone.getTitle())
				.refresh();

		vibrationItem = new SwitchItem(getContext())
				.setChecked(Data.vibrationEnabled)
				.setOnClickListener(this::onVibrationChange)
				.setTitle(getResources().getString(R.string.vibration)).setSubtitle(getResources().getString(Data.vibrationEnabled ? R.string.on : R.string.off))
				.refresh();

		quickAddItem = new SwitchItem(getContext())
				.setChecked(Data.quickAddEnabled)
				.setOnClickListener(this::onQuickAddChange)
				.setTitle(getResources().getString(R.string.quick_add_notification)).setSubtitle(getResources().getString(Data.quickAddEnabled ? R.string.on : R.string.off))
				.refresh();

		quickCategoryItem = new FragmentItem(getContext())
				.setOnFragmentChangeListener(this::onChangeFragment)
				.setFragment(new QuickCategoryFragment().setQuickCategories(Data.quickCategories).setOnQuickCategoryChangeListener(onChangeListener::onQuickCategoryChange))
				.toggle(Data.quickAddEnabled)
				.setTitle(getResources().getString(R.string.quick_category)).setSubtitle(getResources().getString(R.string.customize_quick_categories))
				.refresh();

		synchronizeItem = new FragmentItem(getContext())
				.setOnFragmentChangeListener(fragment -> {
					if(Utils.isOffline(getContext()) && !(settingsList.getChildAt(0) instanceof ErrorMessageView)) {
						settingsList.addView(new ErrorMessageView(getActivity()), ((ViewGroup) synchronizeItem.getParent()).indexOfChild(synchronizeItem));
						TransitionManager.beginDelayedTransition(settingsList, new AutoTransition());
					} else onChangeFragment(fragment);
				})
				.setFragment(new SynchronizeFragment().setOnSynchronizeClickListener(this::onSynchronizeClick))
				.toggle(lastLoggedAccount != null)
				.setTitle(getResources().getString(R.string.synchronize)).setSubtitle(getResources().getString(R.string.save_to_database))
				.refresh();

		signInItem = new ImageItem(getContext())
				.setImageResource(R.mipmap.ic_google_auth)
				.setOnClickListener((DataRunnable<Void>) data -> onSignInClick())
				.setTitle(getResources().getString(lastLoggedAccount == null ? R.string.sign_in : R.string.sign_out))
				.setSubtitle(lastLoggedAccount == null ? getResources().getString(R.string.tap_to_sign_in) : lastLoggedAccount.getDisplayName())
				.refresh();

		settingsList.addView(ringtoneItem);
		settingsList.addView(vibrationItem);
		settingsList.addView(quickAddItem);
		settingsList.addView(quickCategoryItem);
		settingsList.addView(synchronizeItem);
		settingsList.addView(signInItem);
	}

	public void onChangeFragment(Fragment fragment) {
		onChangeListener.onFragmentChange(fragment);
	}

	@Override
	public void onRingtonePick(Ringtone ringtone) {
		ringtoneItem.setSubtitle(ringtone.getTitle()).refreshSubtitle();
		onChangeListener.onFragmentChange(this);
		onChangeListener.onRingtonePick(ringtone);
	}

	public void onVibrationChange(boolean enabled) {
		vibrationItem.setSubtitle(getResources().getString(enabled ? R.string.on : R.string.off)).refreshSubtitle();
		onChangeListener.onVibrationChange(enabled);
	}

	private void onQuickAddChange(boolean enabled) {
		quickAddItem.setSubtitle(getResources().getString(enabled ? R.string.on : R.string.off)).refreshSubtitle();
		quickCategoryItem.toggle(enabled);
		onChangeListener.onQuickAddChange(enabled);
	}

	public void onSignInClick() {
		onChangeListener.onSignInClick();
	}

	public void onSynchronizeClick() {
		onChangeListener.onSynchronizeClick();
	}

	public SettingsListFragment setLastLoggedAccount(GoogleSignInAccount lastLoggedAccount) {
		this.lastLoggedAccount = lastLoggedAccount;
		return this;
	}

	public SettingsListFragment setOnChangeListener(OnChangeListener onChangeListener) {
		this.onChangeListener = onChangeListener;
		return this;
	}

	public interface OnChangeListener {
		void onFragmentChange(Fragment fragment);
		void onRingtonePick(Ringtone ringtone);
		void onVibrationChange(boolean enabled);
		void onQuickAddChange(boolean enabled);
		void onQuickCategoryChange(ArrayList<Category> categories);
		void onSynchronizeClick();
		void onSignInClick();
	}
}
