package com.infinitemind.przypominajka.fragments;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.Switch;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.interfaces.DataRunnable;
import com.infinitemind.przypominajka.interfaces.OnAnimationEndListener;
import com.infinitemind.przypominajka.models.Alarm;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.utils.Constants;
import com.infinitemind.przypominajka.utils.Utils;
import com.infinitemind.przypominajka.views.CustomDatePicker;
import com.infinitemind.przypominajka.views.CustomTimePicker;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlarmAddonFragment extends Fragment implements AddonFragment {

	private int[] dateLimit = new int[]{60, 60, 24, 31};
	private int[] dateIncrease = new int[]{5, 15, 1, 1};
	private int[] dateAmount = new int[4];
	private int[] everyAmountLimit = new int[]{60, 24, 31, 4, 12};
	private boolean getDateRequest;
	private OnAlarmSetListener onAlarmSetListener;
	private ViewGroup rootView;
	private Alarm alarm;

	@Override @Nullable
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_addon_date, container, false);

			handleClicks();
			if(alarm == null)
				alarm = new Alarm();
			setDate(alarm.getDate());
			setEveryAmount();
			setEveryPeriod();
			setRepeatCount();
			setSelectedDays();
			toggleRepeatContainer(alarm.isRepeat());
			((AppCompatTextView) rootView.findViewById(R.id.untilDate)).setText(Utils.dateFormat4.format(alarm.getUntilDate().getTime()));
			if(alarm.isSelectedMode())
				rootView.findViewById(R.id.selectedButton).performClick();
			if(alarm.isUntilMode())
				rootView.findViewById(R.id.arrowRight).performClick();
			if(getDateRequest)
				rootView.findViewById(R.id.buttonsContainer).setVisibility(View.INVISIBLE);
		}

		return rootView;
	}

	private void handleClicks() {
		AppCompatTextView dateButton = rootView.findViewById(R.id.dateButton);
		AppCompatTextView repeatButton = rootView.findViewById(R.id.repeatButton);
		AppCompatTextView everyButton = rootView.findViewById(R.id.everyButton);
		AppCompatTextView selectedButton = rootView.findViewById(R.id.selectedButton);
		AppCompatTextView currentTime = rootView.findViewById(R.id.currentTime);
		AppCompatTextView currentDate = rootView.findViewById(R.id.currentDate);
		AppCompatTextView untilDate = rootView.findViewById(R.id.untilDate);
		ImageView arrowLeft = rootView.findViewById(R.id.arrowLeft);
		ImageView arrowRight = rootView.findViewById(R.id.arrowRight);

		rootView.findViewById(R.id.alarmSetButton).setOnClickListener(view -> {
			Calendar time = Calendar.getInstance(), date = Calendar.getInstance();
			try {
				time.setTime(Utils.dateFormat3.parse(((AppCompatTextView) rootView.findViewById(R.id.currentTime)).getText().toString()));
				date.setTime(Utils.dateFormat4.parse(((AppCompatTextView) rootView.findViewById(R.id.currentDate)).getText().toString()));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
			date.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
			date.set(Calendar.SECOND, 0);
			date.set(Calendar.MILLISECOND, 0);
			boolean repeat = ((Switch) rootView.findViewById(R.id.switchRepeat)).isChecked();
			boolean untilMode = rootView.findViewById(R.id.untilDateContainer).getVisibility() == View.VISIBLE;
			Calendar untilDateCalendar = Calendar.getInstance();
			try {
				untilDateCalendar.setTime(Utils.dateFormat4.parse(untilDate.getText().toString()));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			boolean selectedMode = selectedButton.getCurrentTextColor() == getResources().getColor(R.color.colorAccent1);
			alarm.setDate(date);
			alarm.setRepeat(repeat);
			alarm.setUntilMode(untilMode);
			alarm.setUntilDate(untilDateCalendar);
			alarm.setSelectedMode(selectedMode);
			if(selectedMode) {
				int selectedDays = 0;
				for(int i = 0; i < 7; i++) if(alarm.getSelectedDays()[i]) selectedDays++;
				if(selectedDays == 0) {
					alarm.setRepeat(false);
					toggleRepeatContainer(false);
				}
			}
			onAlarmSetListener.onAlarmSet(alarm);
		});

		dateButton.setOnClickListener(view -> {
			if(dateButton.getCurrentTextColor() != getResources().getColor(R.color.colorAccent1)) {
				dateButton.setTextColor(getResources().getColor(R.color.colorAccent1));
				repeatButton.setTextColor(getResources().getColor(R.color.colorText3));
				animateHorizontal(rootView.findViewById(R.id.dateContainer), true, false);
				animateHorizontal(rootView.findViewById(R.id.repeatContainer), false, false);
			}
		});

		repeatButton.setOnClickListener(view -> {
			if(repeatButton.getCurrentTextColor() != getResources().getColor(R.color.colorAccent1)) {
				dateButton.setTextColor(getResources().getColor(R.color.colorText3));
				repeatButton.setTextColor(getResources().getColor(R.color.colorAccent1));
				animateHorizontal(rootView.findViewById(R.id.dateContainer), false, true);
				animateHorizontal(rootView.findViewById(R.id.repeatContainer), true, true);
			}
		});

		everyButton.setOnClickListener(view -> {
			if(everyButton.getCurrentTextColor() != getResources().getColor(R.color.colorAccent1)) {
				everyButton.setTextColor(getResources().getColor(R.color.colorAccent1));
				selectedButton.setTextColor(getResources().getColor(R.color.colorText3));
				animateHorizontal(rootView.findViewById(R.id.everyContainer), true, false);
				animateHorizontal(rootView.findViewById(R.id.selectedContainer), false, false);
			}
		});

		selectedButton.setOnClickListener(view -> {
			if(selectedButton.getCurrentTextColor() != getResources().getColor(R.color.colorAccent1)) {
				everyButton.setTextColor(getResources().getColor(R.color.colorText3));
				selectedButton.setTextColor(getResources().getColor(R.color.colorAccent1));
				animateHorizontal(rootView.findViewById(R.id.everyContainer), false, true);
				animateHorizontal(rootView.findViewById(R.id.selectedContainer), true, true);
			}
		});

		arrowRight.setOnClickListener(view -> {
			arrowLeft.setVisibility(View.VISIBLE);
			arrowRight.setVisibility(View.GONE);
			animateHorizontal(rootView.findViewById(R.id.untilDateContainer), true, true);
			animateHorizontal(rootView.findViewById(R.id.repeatCountContainer), false, true);
			try {
				Date date = Utils.dateFormat4.parse(currentDate.getText().toString());
				Date temp = Utils.dateFormat4.parse(untilDate.getText().toString());
				if(temp.before(date))
					untilDate.setText(Utils.dateFormat4.format(date));
			} catch(ParseException e) {
				e.printStackTrace();
			}
		});

		arrowLeft.setOnClickListener(view -> {
			arrowLeft.setVisibility(View.GONE);
			arrowRight.setVisibility(View.VISIBLE);
			animateHorizontal(rootView.findViewById(R.id.untilDateContainer), false, false);
			animateHorizontal(rootView.findViewById(R.id.repeatCountContainer), true, false);
		});

		currentTime.setOnClickListener(view -> showTimePickerDialog(currentTime.getText().toString(), date -> {
			currentTime.setText(Utils.dateFormat3.format(date.getTime()));
			refreshButtonsAmount();
		}));

		currentDate.setOnClickListener(view -> showDatePickerDialog(Utils.dateFormat4.format(Calendar.getInstance().getTime()), currentDate.getText().toString(), date -> {
			currentDate.setText(Utils.dateFormat4.format(date.getTime()));
			try {
				Date temp = Utils.dateFormat4.parse(untilDate.getText().toString());
				if(temp.before(date.getTime()))
					untilDate.setText(Utils.dateFormat4.format(date.getTime()));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			refreshButtonsAmount();
		}));

		untilDate.setOnClickListener(view -> showDatePickerDialog(currentDate.getText().toString(), untilDate.getText().toString(), date -> {
			untilDate.setText(Utils.dateFormat4.format(date.getTime()));
		}));

		for(int i = 0; i < 4; i++) {
			int finalI = i;
			rootView.findViewById(getResources().getIdentifier("dateContainer" + i, "id", getContext().getPackageName())).setOnClickListener(view -> {
				dateAmount[finalI] += dateIncrease[finalI];
				setButtonAmount(finalI);
				Calendar date = setDate();
				try {
					Date temp = Utils.dateFormat4.parse(untilDate.getText().toString());
					if(temp.before(date.getTime()))
						untilDate.setText(Utils.dateFormat4.format(date.getTime()));
				} catch(ParseException e) {
					e.printStackTrace();
				}
			});
			rootView.findViewById(getResources().getIdentifier("removeContainer" + i, "id", getContext().getPackageName())).setOnClickListener(view -> {
				dateAmount[finalI] -= dateIncrease[finalI];
				setButtonAmount(finalI);
				setDate();
			});
		}

		rootView.findViewById(R.id.repeatCount).setOnClickListener(view -> {
			alarm.setRepeatCount(alarm.getRepeatCount() + 1);
			setRepeatCount();
		});

		rootView.findViewById(R.id.repeatRemoveContainer).setOnClickListener(view -> {
			if(alarm.getRepeatCount() > 0) {
				alarm.setRepeatCount(alarm.getRepeatCount() - 1);
				setRepeatCount();
			}
		});

		rootView.findViewById(R.id.everyAmount).setOnClickListener(view -> {
			alarm.setEveryAmount(alarm.getEveryAmount() + 1);
			if(alarm.getEveryAmount() > everyAmountLimit[alarm.getEveryPeriod()])
				alarm.setEveryAmount(1);
			setEveryAmount();
			setEveryPeriod();
		});

		rootView.findViewById(R.id.everyRemoveContainer).setOnClickListener(view -> {
			if(alarm.getEveryAmount() > 1)
				alarm.setEveryAmount(alarm.getEveryAmount() - 1);
			setEveryAmount();
			setEveryPeriod();
		});

		rootView.findViewById(R.id.everyPeriod).setOnClickListener(view -> {
			alarm.setEveryPeriod(alarm.getEveryPeriod() + 1);
			if(alarm.getEveryPeriod() >= everyAmountLimit.length)
				alarm.setEveryPeriod(0);
			alarm.setEveryAmount(1);
			setEveryAmount();
			setEveryPeriod();
		});

		for(int i = 0; i < 7; i++) {
			int finalI = i;
			rootView.findViewById(getResources().getIdentifier("weekday" + i, "id", getContext().getPackageName())).setOnClickListener(view -> {
				alarm.getSelectedDays()[finalI] = !alarm.getSelectedDays()[finalI];
				setSelectedDays(finalI);
			});
		}

		((Switch) rootView.findViewById(R.id.switchRepeat)).setOnCheckedChangeListener((compoundButton, b) -> toggleRepeatContainer(b));
	}

	private void toggleRepeatContainer(boolean clickable) {
		((Switch) rootView.findViewById(R.id.switchRepeat)).setChecked(clickable);
		rootView.findViewById(R.id.switchRepeatContainer).setAlpha(clickable ? 1 : 0.3f);
		if(clickable) Utils.enableChildrenClicks(rootView.findViewById(R.id.switchRepeatContainer));
		else Utils.disableChildrenClicks(rootView.findViewById(R.id.switchRepeatContainer));
	}

	private void setSelectedDays(int... index) {
		String packageName = getContext().getPackageName();
		for(int i = index.length > 0 ? index[0] : 0; i <= (index.length > 0 ? index[0] : 6); i++) {
			CircleImageView border = rootView.findViewById(getResources().getIdentifier("weekdayBorder" + i, "id", packageName));
			AppCompatTextView weekday = rootView.findViewById(getResources().getIdentifier("weekday" + i, "id", packageName));
			int color = getResources().getColor(alarm.getSelectedDays()[i] ? R.color.colorAccent1 : R.color.colorText3);
			weekday.setTextColor(color);
			border.setBorderColor(color);
		}
	}

	private void showDatePickerDialog(String minDate, String source, DataRunnable<Calendar> callback) {
		Utils.makeDialog(getActivity(), R.layout.dialog_date_picker, dialog -> {
			Calendar calendar = Calendar.getInstance();
			long minDateLong = 0;
			try {
				calendar.setTime(Utils.dateFormat4.parse(source));
				minDateLong = Utils.dateFormat4.parse(minDate).getTime();
			} catch(ParseException e) {
				e.printStackTrace();
			}
			CustomDatePicker datePicker = dialog.findViewById(R.id.datePicker);
			datePicker.setMinDate(minDateLong);
			datePicker.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
			dialog.findViewById(R.id.doneButton).setOnClickListener(view1 -> {
				calendar.set(Calendar.YEAR, datePicker.getYear());
				calendar.set(Calendar.MONTH, datePicker.getMonth());
				calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
				callback.run(calendar);
				dialog.dismiss();
			});
			dialog.findViewById(R.id.cancelButton).setOnClickListener(view1 -> dialog.dismiss());
		});
	}

	private void showTimePickerDialog(String source, DataRunnable<Calendar> callback) {
		Utils.makeDialog(getActivity(), R.layout.dialog_time_picker, dialog -> {
			Calendar calendar = Calendar.getInstance();
			try {
				calendar.setTime(Utils.dateFormat3.parse(source));
			} catch(ParseException e) {
				e.printStackTrace();
			}
			CustomTimePicker timePicker = dialog.findViewById(R.id.timePicker);
			timePicker.setIs24HourView(true);
			timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
			timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));

			dialog.findViewById(R.id.doneButton).setOnClickListener(view1 -> {
				calendar.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
				calendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());
				callback.run(calendar);
				dialog.dismiss();
			});
			dialog.findViewById(R.id.cancelButton).setOnClickListener(view1 -> dialog.dismiss());
		});
	}

	private void setEveryPeriod() {
		((AppCompatTextView) rootView.findViewById(R.id.everyPeriod)).setText(getResources().getQuantityString(
				getResources().getIdentifier("everyPeriod" + alarm.getEveryPeriod(), "plurals", getContext().getPackageName()), alarm.getEveryAmount()));
	}

	private void setEveryAmount() {
		rootView.findViewById(R.id.everyRemoveContainer).setVisibility(alarm.getEveryAmount() == 1 ? View.GONE : View.VISIBLE);
		((AppCompatTextView) rootView.findViewById(R.id.everyAmount)).setText(String.valueOf(alarm.getEveryAmount()));
		((AppCompatTextView) rootView.findViewById(R.id.everyAmount)).setTextColor(getResources().getColor(alarm.getEveryAmount() == 1 ? R.color.colorText3 : R.color.colorAccent1));
		((CircleImageView) rootView.findViewById(R.id.everyAmountBorder)).setBorderColor(getResources().getColor(alarm.getEveryAmount() == 1 ? R.color.colorText3 : R.color.colorAccent1));
	}

	private void setRepeatCount() {
		AppCompatTextView repeatCount = rootView.findViewById(R.id.repeatCount);
		AppCompatTextView repeatTimes = rootView.findViewById(R.id.repeatTimes);
		CircleImageView repeatBorder = rootView.findViewById(R.id.repeatCountBorder);
		ViewGroup removeContainer = rootView.findViewById(R.id.repeatRemoveContainer);
		if(alarm.getRepeatCount() == 0) {
			repeatBorder.setBorderColor(getResources().getColor(R.color.colorText3));
			repeatCount.setTextColor(getResources().getColor(R.color.colorText3));
			removeContainer.setVisibility(View.GONE);
			repeatCount.setText("∞");
		} else {
			repeatBorder.setBorderColor(getResources().getColor(R.color.colorAccent1));
			repeatCount.setTextColor(getResources().getColor(R.color.colorAccent1));
			removeContainer.setVisibility(View.VISIBLE);
			repeatCount.setText(String.valueOf(alarm.getRepeatCount()));
		}
		repeatTimes.setText(getResources().getQuantityString(R.plurals.times, alarm.getRepeatCount()));
	}

	private void setButtonAmount(int index) {
		Resources resources = getResources();
		String packageName = getContext().getPackageName();
		AppCompatTextView dateValue = rootView.findViewById(resources.getIdentifier("dateValue" + index, "id", packageName));
		AppCompatTextView dateDescription = rootView.findViewById(resources.getIdentifier("dateDescription" + index, "id", packageName));
		CircleImageView dateBorder = rootView.findViewById(resources.getIdentifier("dateBorder" + index, "id", packageName));
		ViewGroup removeContainer = rootView.findViewById(resources.getIdentifier("removeContainer" + index, "id", packageName));

		if(dateAmount[index] > dateLimit[index])
			dateAmount[index] = 0;

		removeContainer.setVisibility(dateAmount[index] == 0 ? View.GONE : View.VISIBLE);

		dateValue.setText(String.valueOf(dateAmount[index] == 0 ? dateIncrease[index] : dateAmount[index]));
		dateValue.setTextColor(resources.getColor(dateAmount[index] == 0 ? R.color.colorText3 : R.color.colorAccent1));
		dateBorder.setBorderColor(resources.getColor(dateAmount[index] == 0 ? R.color.colorText3 : R.color.colorAccent1));
		dateDescription.setTextColor(resources.getColor(dateAmount[index] == 0 ? R.color.colorText3 : R.color.colorAccent1));
	}

	private void refreshButtonsAmount() {
		Calendar now = Calendar.getInstance();
		Calendar chosen = Calendar.getInstance();
		Calendar time = Calendar.getInstance(), date = Calendar.getInstance();
		try {
			time.setTime(Utils.dateFormat3.parse(((AppCompatTextView) rootView.findViewById(R.id.currentTime)).getText().toString()));
			date.setTime(Utils.dateFormat4.parse(((AppCompatTextView) rootView.findViewById(R.id.currentDate)).getText().toString()));
		} catch(ParseException e) {
			e.printStackTrace();
		}
		chosen.set(Calendar.YEAR, date.get(Calendar.YEAR));
		chosen.set(Calendar.MONTH, date.get(Calendar.MONTH));
		chosen.set(Calendar.DAY_OF_MONTH, date.get(Calendar.DAY_OF_MONTH));
		chosen.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
		chosen.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
		chosen.set(Calendar.SECOND, 0);
		chosen.set(Calendar.MILLISECOND, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		if(chosen.after(now)) {
			int days = (int) TimeUnit.MILLISECONDS.toDays(chosen.getTimeInMillis() - now.getTimeInMillis());
			if(days != 0 && days <= dateLimit[3]) {
				dateAmount[3] = days;
				chosen.add(Calendar.DAY_OF_MONTH, -days);
			} else dateAmount[3] = 0;

			int hours = (int) TimeUnit.MILLISECONDS.toHours(chosen.getTimeInMillis() - now.getTimeInMillis());
			if(hours != 0 && hours <= dateLimit[2]) {
				dateAmount[2] = hours;
				chosen.add(Calendar.HOUR_OF_DAY, -hours);
			} else dateAmount[2] = 0;

			int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(chosen.getTimeInMillis() - now.getTimeInMillis());
			if(minutes > 0 && minutes <= dateLimit[1]) {
				if(dateIncrease[1] < minutes && minutes % dateIncrease[0] == 0) {
					dateAmount[1] = (int) Math.floor((float) minutes / dateIncrease[1]) * dateIncrease[1];
					chosen.add(Calendar.MINUTE, -dateAmount[1]);
					minutes -= dateAmount[1];

					dateAmount[0] = (int) Math.floor((float) minutes / dateIncrease[0]) * dateIncrease[0];
					chosen.add(Calendar.MINUTE, -dateAmount[0]);
				} else if(minutes % dateIncrease[1] == 0)
					dateAmount[1] = minutes / dateIncrease[1];
				else {
					dateAmount[1] = 0;
					if(minutes % dateIncrease[0] == 0)
						dateAmount[0] = (int) Math.floor((float) minutes / dateIncrease[0]) * dateIncrease[0];
					else dateAmount[0] = 0;
				}
			}
			for(int i = 0; i < 4; i++)
				setButtonAmount(i);
		} else for(int i = 0; i < 4; i++) {
			dateAmount[i] = 0;
			setButtonAmount(i);
		}
	}

	private Calendar setDate(Calendar... date) {
		Calendar calendar = date.length > 0 ? date[0] : Calendar.getInstance();
		calendar.add(Calendar.MINUTE, dateAmount[0] + dateAmount[1]);
		calendar.add(Calendar.HOUR, dateAmount[2]);
		calendar.add(Calendar.DAY_OF_MONTH, dateAmount[3]);
		((AppCompatTextView) rootView.findViewById(R.id.currentTime)).setText(Utils.dateFormat3.format(calendar.getTime()));
		((AppCompatTextView) rootView.findViewById(R.id.currentDate)).setText(Utils.dateFormat4.format(calendar.getTime()));
		((AppCompatTextView) rootView.findViewById(R.id.currentDayOfWeek)).setText(Utils.dateFormat5.format(calendar.getTime()));
		return calendar;
	}

	private void animateHorizontal(View view, boolean show, boolean left) {
		view.setVisibility(View.VISIBLE);
		ObjectAnimator a = ObjectAnimator.ofFloat(view, "alpha", show ? 0 : 1, show ? 1 : 0);
		ObjectAnimator tX = ObjectAnimator.ofFloat(view, "translationX", show ? (left ? 50 : -50) : 0, show ? 0 : (left ? -50 : 50));
		AnimatorSet set = new AnimatorSet();
		set.playTogether(a, tX);
		set.setDuration(Constants.DURATION).setInterpolator(new DecelerateInterpolator(2f));
		set.start();
		set.addListener(new OnAnimationEndListener(animator -> view.setVisibility(show ? View.VISIBLE : View.INVISIBLE)));
	}

	@Override @DrawableRes
	public int getIcon() {
		return R.drawable.ic_alarm;
	}

	public AlarmAddonFragment setAlarm(Alarm alarm) {
		this.alarm = alarm;
		return this;
	}

	public AlarmAddonFragment getDate() {
		getDateRequest = true;
		return this;
	}

	public AlarmAddonFragment setOnAlarmSetListener(OnAlarmSetListener onAlarmSetListener) {
		this.onAlarmSetListener = onAlarmSetListener;
		return this;
	}

	public interface OnAlarmSetListener {
		void onAlarmSet(Alarm date);
	}
}