package com.infinitemind.przypominajka.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.settings.ButtonItem;
import com.infinitemind.przypominajka.settings.SettingsItem;
import com.infinitemind.przypominajka.settings.SwitchItem;
import com.infinitemind.przypominajka.utils.Utils;
import com.infinitemind.przypominajka.views.ErrorMessageView;

public class SynchronizeFragment extends Fragment {

	private ViewGroup rootView;
	private Context context;
	private Runnable onSynchronizeClickListener;
	public ButtonItem synchronizeButtonItem;
	public SettingsItem mergeLocally;
	public SettingsItem mergeConflicts;
	public SettingsItem cleanUpDatabase;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_synchronize, container, false);

			Data.loadSettings(context = getContext());
			setSettingsList();
		}

		return rootView;
	}

	private void setSettingsList() {
		ViewGroup settingsList = rootView.findViewById(R.id.settingItemContainer);

		cleanUpDatabase = new SwitchItem(getContext())
				.setChecked(Data.cleanUpDatabase)
				.setOnClickListener(this::onUpdateModeClick)
				.setTitle(getResources().getString(R.string.clean_up_database)).setSubtitle(getResources().getString(R.string.upload_only_local_data)).refresh();
		mergeConflicts = new SwitchItem(getContext())
				.setChecked(Data.mergeConflicts)
				.toggle(!Data.cleanUpDatabase)
				.setOnClickListener(this::onMergeConflictsClick)
				.setTitle(getResources().getString(R.string.merge_conflicts)).setSubtitle(getResources().getString(R.string.merge_if_conflicts)).refresh();
		mergeLocally = new SwitchItem(getContext())
				.setChecked(Data.mergeLocally)
				.toggle(!Data.cleanUpDatabase && Data.mergeConflicts)
				.setOnClickListener(this::onMergeLocallyClick)
				.setTitle(getResources().getString(R.string.merge_locally)).setSubtitle(getResources().getString(R.string.resolve_conflicts_locally)).refresh();
		synchronizeButtonItem = new ButtonItem(getContext()).setOnClickListener(() -> {
			if(Utils.isOffline(getContext()) && !(settingsList.getChildAt(0) instanceof ErrorMessageView)) {
				settingsList.addView(new ErrorMessageView(getActivity()), 0);
				TransitionManager.beginDelayedTransition(settingsList, new AutoTransition());
			} else onSynchronizeClick();
		}).setContent(getResources().getString(R.string.synchronize)).setColor(getResources().getColor(R.color.colorAccent1)).refresh();

		settingsList.addView(cleanUpDatabase);
		settingsList.addView(mergeConflicts);
		settingsList.addView(mergeLocally);
		settingsList.addView(synchronizeButtonItem);
	}

	public void onSynchronizeClick() {
		onSynchronizeClickListener.run();
	}

	public SynchronizeFragment setOnSynchronizeClickListener(Runnable onSynchronizeClickListener) {
		this.onSynchronizeClickListener = onSynchronizeClickListener;
		return this;
	}

	private void onMergeConflictsClick(boolean enabled) {
		Data.mergeConflicts = enabled;
		Data.saveSettings(context);
		mergeLocally.toggle(enabled);
	}

	private void onMergeLocallyClick(boolean enabled) {
		Data.mergeLocally = enabled;
		Data.saveSettings(context);
	}

	private void onUpdateModeClick(boolean enabled) {
		Data.cleanUpDatabase = enabled;
		Data.saveSettings(context);
		mergeConflicts.toggle(!enabled);
		mergeLocally.toggle(!enabled && Data.mergeConflicts);
	}
}
