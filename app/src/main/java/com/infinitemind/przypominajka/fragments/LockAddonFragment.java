package com.infinitemind.przypominajka.fragments;

import android.support.annotation.DrawableRes;
import android.support.annotation.IntDef;
import android.support.v4.app.Fragment;

import com.infinitemind.przypominajka.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LockAddonFragment extends Fragment implements AddonFragment {

	@Retention(RetentionPolicy.SOURCE)
	@IntDef(flag = true, value = {UNLOCKED, LOCKED, FULLSCREEN})
	public @interface LockState { }

	@LockState public int state;
	public static final int UNLOCKED = 0;
	public static final int LOCKED = 1;
	public static final int FULLSCREEN = 1 << 1;
	private OnLockChangeListener onLockChangeListener;

	public LockAddonFragment setLock(@LockState int lockState) {
		this.state = lockState;
		return this;
	}

	@Override
	public boolean hasStates() {
		return true;
	}

	@Override
	public int getMaxStates() {
		return 2;
	}

	@Override
	public void changeState() {
		this.state = (this.state + 1) % getMaxStates();
		if(onLockChangeListener != null)
			onLockChangeListener.onLockChange(this.state);
	}

	@Override
	@DrawableRes
	public int getIcon() {
		switch(this.state) {
			case UNLOCKED:
				return R.drawable.ic_lock_open;
			case LOCKED:
				return R.drawable.ic_lock;
			case FULLSCREEN:
				return R.drawable.ic_fullscreen;
			default:
				return 0;
		}
	}

	public LockAddonFragment setOnLockChangeListener(OnLockChangeListener onLockChangeListener) {
		this.onLockChangeListener = onLockChangeListener;
		return this;
	}

	public interface OnLockChangeListener {
		void onLockChange(@LockState int state);
	}
}
