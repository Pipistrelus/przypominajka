package com.infinitemind.przypominajka.fragments;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.ColorsListAdapter;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Color;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Selectable;
import com.infinitemind.przypominajka.utils.Utils;
import com.infinitemind.przypominajka.views.CustomFlagView;
import com.skydoves.colorpickerpreference.ColorPickerView;

import java.util.ArrayList;

public class ColorAddonFragment extends Fragment implements AddonFragment {

	private OnColorPickListener onColorPickListener;
	private ColorsListAdapter colorsListAdapter;
	private ArrayList<Color> colors;
	private ViewGroup rootView;
	private int color;

	@Override @Nullable
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_addon_color, container, false);
			setColorList();
		}

		if(color == 0)
			color = getResources().getColor(R.color.colorAccent1);
		Selectable.selectOnlyPosition(colors, Color.indexOf(colors, color));
		if(colorsListAdapter != null)
			colorsListAdapter.notifyDataSetChanged();

		return rootView;
	}

	private void setColorList() {
		colors = new ArrayList<>(Data.allColors);
		colors.add(new Color(-1));
		((RecyclerView) rootView.findViewById(R.id.colorsList)).setLayoutManager(new GridLayoutManager(getContext(), Utils.getScreenSize(getContext()).x / Utils.dpToPx(getContext(), 75)));
		((RecyclerView) rootView.findViewById(R.id.colorsList)).setAdapter(colorsListAdapter = new ColorsListAdapter(colors));
		colorsListAdapter.setOnItemClickListener(new ColorsListAdapter.MyClickListener() {
			@Override public void onItemClick(int position) {
				if(position != colors.size() - 1) {
					Selectable.selectOnlyPosition(colors, position);
					colorsListAdapter.notifyDataSetChanged();
					if(onColorPickListener != null)
						onColorPickListener.onColorPick(colors.get(position).getColor());
				} else Color.showAddColorDialog(getActivity(), dialog -> {
					Color color = new Color(((ColorPickerView) dialog.findViewById(R.id.colorPicker)).getColor());
					colors.add(colors.size() - 1, color);
					Data.allColors.add(color);
					Data.customColorsAmount++;
					Data.saveColors(getContext());
					onColorPickListener.onColorPick(color.getColor());
					dialog.dismiss();
				});
			}
			@Override public void onItemLongClick(int position, Color color) {
				if(position != colors.size() - 1 && position >= Data.allColors.size() - Data.customColorsAmount)
					Color.showDeleteColorDialog(getActivity(), dialog -> {
						colors.remove(position);
						colorsListAdapter.notifyDataSetChanged();
						Data.allColors.remove(color);
						Data.customColorsAmount--;
						Data.saveColors(getContext());
						onColorPickListener.onColorRemove(color.getColor());
						dialog.dismiss();
					});
			}
		});
	}

	public ColorAddonFragment setOnColorPickListener(OnColorPickListener onColorPickListener) {
		this.onColorPickListener = onColorPickListener;
		return this;
	}

	public ColorAddonFragment setColor(int color) {
		this.color = color;
		return this;
	}

	@Override @DrawableRes
	public int getIcon() {
		return R.drawable.ic_color_palette;
	}

	public interface OnColorPickListener {
		void onColorPick(int color);
		void onColorRemove(int color);
	}
}
