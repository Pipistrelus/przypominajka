package com.infinitemind.przypominajka.fragments;

import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.widget.ImageView;

public interface AddonFragment {

	default ShowType getShowType() {
		return ShowType.always;
	}

	default boolean hasStates() {
		return false;
	}

	default int getMaxStates() {
		return 0;
	}

	default void changeState() {}

	@DrawableRes int getIcon();

	enum ShowType {
		always, ifPossible, never
	}
}
