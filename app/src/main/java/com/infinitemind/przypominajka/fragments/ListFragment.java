package com.infinitemind.przypominajka.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.ColorsListAdapter;
import com.infinitemind.przypominajka.adapters.NotesListAdapter;
import com.infinitemind.przypominajka.alarms.Alarms;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.models.SectionHeader;
import com.infinitemind.przypominajka.utils.BottomPaddingItemDecoration;
import com.infinitemind.przypominajka.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class ListFragment extends Fragment {

	private ViewGroup rootView;
	private NotesListAdapter notesListAdapter;
	private OnNoteClickListener onNoteClickListener;
	private ArrayList<SectionHeader> sections;
	public boolean wholeListShowed = true;
	public SectionHeader selectedSection;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_list, container, false);

			setNotesList();
			refreshStatistics();
			handleClicks();

			Toolbar toolbar = rootView.findViewById(R.id.toolbar);
			toolbar.inflateMenu(R.menu.menu_filter);
			toolbar.setOnMenuItemClickListener(menuItem -> {
				if(menuItem.getItemId() == R.id.action_filter) {
					clickFilter();
					return true;
				}
				return false;
			});

			((AppBarLayout) rootView.findViewById(R.id.app_bar)).addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
				if(appBarLayout.getTotalScrollRange() + verticalOffset == 0) {
//					toggleAction(toolbar, true);
					toggleToolbarTitle(true);
					toggleFilters(false);
				} else toggleAction(toolbar, false);
			});
		}

		return rootView;
	}

	private void toggleAction(Toolbar toolbar, boolean visibility) {
		MenuItem item = toolbar.getMenu().findItem(R.id.action_filter);
		if(item.isVisible() != visibility) item.setVisible(!item.isVisible());
	}

	private void toggleToolbar(boolean expanded) {
		rootView.<AppBarLayout>findViewById(R.id.app_bar).setExpanded(expanded, true);
	}

	private void toggleToolbarTitle(boolean enabled) {
		rootView.<CollapsingToolbarLayout>findViewById(R.id.toolbar_layout).setTitleEnabled(enabled);
	}

	private void toggleList(int id, boolean enabled) {
		rootView.findViewById(id).setVisibility(enabled ? View.VISIBLE : View.GONE);

		TransitionManager.beginDelayedTransition(rootView.findViewById(R.id.toolbar_layout), new AutoTransition());
	}

	private void handleClicks() {
		rootView.findViewById(R.id.buttonFilter).setOnClickListener(view -> clickFilter());
		rootView.findViewById(R.id.categoryIcon).setOnClickListener(view -> {
			if(selectedSection != null && selectedSection.getCategory().getIcon() != null)
				Category.showCategoryDialog(getActivity(), selectedSection.getCategory(), category -> {
					Data.loadCategories(getContext());
					refreshStatistics();
				});
		});
		rootView.findViewById(R.id.buttonByColors).setOnClickListener(v -> {
			RecyclerView list = rootView.findViewById(R.id.listByColors);
			list.setLayoutManager(new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.HORIZONTAL, false));
			list.setAdapter(new ColorsListAdapter(Data.allColors));

			toggleList(R.id.listByColors, true);
		});
		rootView.findViewById(R.id.buttonByDate).setOnClickListener(v -> {
			toggleList(R.id.listByDate, true);
		});
	}

	private void clickFilter() {
		toggleToolbar(true);
		toggleToolbarTitle(false);
		toggleFilters(true);
	}

	private void toggleFilters(boolean enabled) {
		rootView.findViewById(R.id.filtersContainer).setVisibility(enabled ? View.VISIBLE : View.GONE);
	}

	private void refreshStatistics() {
		Resources resources = rootView.getContext().getResources();
		((AppCompatTextView) rootView.findViewById(R.id.textTasks)).setText(resources.getString(R.string.tasks, SectionHeader.countNotes(sections)));
		((AppCompatTextView) rootView.findViewById(R.id.textUncompleted)).setText(resources.getString(R.string.uncompleted, Note.countUnchecked(SectionHeader.getNotes(sections))));
		if(selectedSection != null) {
			((CollapsingToolbarLayout) rootView.findViewById(R.id.toolbar_layout)).setTitle(selectedSection.getCategory().getName());
			((AppCompatTextView) rootView.findViewById(R.id.categoryIcon)).setText(selectedSection.getCategory().getIcon());
			rootView.findViewById(R.id.statisticsContainer).setVisibility(View.VISIBLE);
		} else {
			((CollapsingToolbarLayout) rootView.findViewById(R.id.toolbar_layout)).setTitle(resources.getString(R.string.app_name));
			rootView.findViewById(R.id.statisticsContainer).setVisibility(View.GONE);
		}
	}

	private void setNotesList() {
		RecyclerView notesList = rootView.findViewById(R.id.notesList);
		notesList.setLayoutManager(new LinearLayoutManager(rootView.getContext(), LinearLayoutManager.VERTICAL, false));
		notesList.setAdapter(notesListAdapter = new NotesListAdapter(rootView.getContext(), sections = getSections()));
		notesList.addItemDecoration(new BottomPaddingItemDecoration(Utils.dpToPx(rootView.getContext(), 100)));
		notesListAdapter.setOnItemClickListener(new NotesListAdapter.MyClickListener() {
			@Override public void onHeaderClick(int sectionPosition) {
				if(sections.size() > 1 || !wholeListShowed) {
					wholeListShowed = !wholeListShowed;
					if(!wholeListShowed) {
						selectedSection = sections.get(sectionPosition);
						sections.clear();
						sections.add(selectedSection);
						selectedSection.setShow(false);
					} else {
						selectedSection = null;
						sections.clear();
						sections.addAll(getSections());
					}
					refreshStatistics();
					toggleToolbar(false);
					new Handler().post(() -> notesListAdapter.notifyDataChanged(sections));
				}
			}
			@Override public void onItemClick(int sectionPosition, int position) {
				if(onNoteClickListener != null)
					onNoteClickListener.onNoteClick(sections.get(sectionPosition).getChildItems().get(position).getId());
			}
			@Override public void onItemChecked(int sectionPosition, int position, boolean checked) {
				if(sectionPosition < sections.size()) {
					Note note = sections.get(sectionPosition).getChildItems().get(position);
					note.setChecked(checked);
					Data.saveData(getContext());
					if(checked) Alarms.cancelAlarm(getContext(), note);
					else Alarms.setAlarm(getContext(), note, true);
					new Handler().post(() -> notesListAdapter.notifyDataChanged(sections));
					refreshStatistics();
				}
			}
		});
	}

	private ArrayList<SectionHeader> getSections() {
		return getSections(Data.groupType);
	}

	private ArrayList<SectionHeader> getSections(GroupType type) {
		ArrayList<SectionHeader> sections = new ArrayList<>();

		if(type == GroupType.CATEGORY) sections = getSectionsByCategory();
		else if(type == GroupType.DATE) sections = getSectionsByDate();

		for(int i = sections.size() - 1; i >= 0; i--) {
			SectionHeader sectionHeader = sections.get(i);
			if(sectionHeader.getChildItems().isEmpty())
				sections.remove(sectionHeader);
		}
		return sections;
	}

	private ArrayList<SectionHeader> getSectionsByCategory() {
		ArrayList<SectionHeader> sections = new ArrayList<>();
		HashMap<UUID, Integer> pos = new HashMap<>();
		ArrayList<Note> theRest = new ArrayList<>();
		for(int i = 0; i < Data.allCategories.size(); i++) {
			sections.add(new SectionHeader(new ArrayList<>(), Data.allCategories.get(i)));
			pos.put(Data.allCategories.get(i).getId(), i);
		}
		for(Note note : new ArrayList<>(Data.allNotes)) {
			if(pos.containsKey(note.getCategoryID()))
				sections.get(pos.get(note.getCategoryID())).getChildItems().add(note);
			else theRest.add(note);
		}
		if(!theRest.isEmpty()) {
			Category category = new Category(getResources().getString(R.string.without_category), getResources().getColor(R.color.colorAccent1), null);
			category.setId(UUID.nameUUIDFromBytes(category.getName().getBytes()));
			sections.add(new SectionHeader(theRest, category));
		}

		Collections.sort(sections, (section1, section2) -> section1.getCategory().getName().compareTo(section2.getCategory().getName()));
		return sections;
	}

	private ArrayList<SectionHeader> getSectionsByDate() {
		ArrayList<SectionHeader> sections = new ArrayList<>();
		HashMap<String, Integer> pos = new HashMap<>();
		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.MILLISECOND, 0);

		ArrayList<Note> allNotes = new ArrayList<>(Data.allNotes);

		Collections.sort(allNotes, (note1, note2) -> note1.getAlarm().getDate().compareTo(note2.getAlarm().getDate()));

		for(Note note : allNotes) {
			long days = TimeUnit.MILLISECONDS.toDays(note.getAlarm().getDate().getTimeInMillis() - now.getTimeInMillis());
			String date;

			if(note.getAlarm().getDate().before(now)) date = getResources().getString(R.string.older);
			else if(days == 0) date = getResources().getString(R.string.today);
			else if(days == 1) date = getResources().getString(R.string.tomorrow);
			else if(days <= 7) date = getResources().getString(R.string.in_a_week);
			else date = Utils.dateFormat2.format(note.getAlarm().getDate().getTime());

			if(!pos.containsKey(date)) {
				pos.put(date, sections.size());
				Category category = new Category(date, note.getColor(), null);
				category.setId(UUID.nameUUIDFromBytes(date.getBytes()));
				sections.add(new SectionHeader(new ArrayList<>(), category));
			}
		}
		for(Note note : allNotes) {
			long days = TimeUnit.MILLISECONDS.toDays(note.getAlarm().getDate().getTimeInMillis() - now.getTimeInMillis());
			String date;
			if(note.getAlarm().getDate().before(now)) date = getResources().getString(R.string.older);
			else if(days == 0) date = getResources().getString(R.string.today);
			else if(days == 1) date = getResources().getString(R.string.tomorrow);
			else if(days <= 7) date = getResources().getString(R.string.in_a_week);
			else date = Utils.dateFormat2.format(note.getAlarm().getDate().getTime());

			sections.get(pos.get(date)).getChildItems().add(note);
		}
		return sections;
	}

	public void refreshList() {
		if(notesListAdapter != null) {
			ArrayList<SectionHeader> sections = getSections();
			if(wholeListShowed && selectedSection != null) selectedSection = null;
			if(this.sections == null) {
				if(sections.size() == 1) {
					selectedSection = sections.get(0);
					selectedSection.setShow(false);
				}
				if(selectedSection == null)
					this.sections = sections;
				else {
					this.sections = new ArrayList<>();
					this.sections.add(selectedSection);
				}
			} else {
				this.sections.clear();
				if(sections.size() == 1) {
					selectedSection = sections.get(0);
					selectedSection.setShow(false);
				}
				if(selectedSection == null)
					this.sections.addAll(sections);
				else this.sections.add(selectedSection);
			}
			notesListAdapter.notifyDataChanged(this.sections);
			refreshStatistics();
		}
	}

	public ListFragment setOnNoteClickListener(OnNoteClickListener onNoteClickListener) {
		this.onNoteClickListener = onNoteClickListener;
		return this;
	}

	public interface OnNoteClickListener {
		void onNoteClick(UUID noteID);
	}

	public enum GroupType {
		CATEGORY, DATE;

		public int getIcon() {
			switch(ordinal()) {
				case 0: return R.drawable.ic_home;
				case 1: return R.drawable.ic_event;
			}
			return 0;
		}
	}
}
