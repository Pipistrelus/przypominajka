package com.infinitemind.przypominajka.fragments;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.infinitemind.przypominajka.R;

public class SilentAddonFragment extends Fragment implements AddonFragment {

	private OnSilentChangeListener onSilentChangeListener;
	private boolean silent;

	@Override
	@Nullable
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return null;
	}

	public SilentAddonFragment setSilent(boolean silent) {
		this.silent = silent;
		return this;
	}

	@Override
	public boolean hasStates() {
		return true;
	}

	@Override
	public int getMaxStates() {
		return 2;
	}

	@Override
	public void changeState() {
		silent = !silent;
		if(onSilentChangeListener != null)
			onSilentChangeListener.onSilentChange(silent);
	}

	@Override
	@DrawableRes
	public int getIcon() {
		return silent ? R.drawable.ic_silent : R.drawable.ic_not_silent;
	}

	public SilentAddonFragment setOnLockChangeListener(OnSilentChangeListener onSilentChangeListener) {
		this.onSilentChangeListener = onSilentChangeListener;
		return this;
	}

	public interface OnSilentChangeListener {
		void onSilentChange(boolean silent);
	}
}
