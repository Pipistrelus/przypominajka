package com.infinitemind.przypominajka.fragments;

import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.RingtoneListAdapter;
import com.infinitemind.przypominajka.models.Ringtone;
import com.infinitemind.przypominajka.models.Selectable;

import java.util.ArrayList;

public class RingtoneListFragment extends Fragment {

	private OnRingtonePickListener onRingtonePickListener;
	private android.media.Ringtone playingRingtone;
	private ArrayList<Ringtone> ringtones;
	private ViewGroup rootView;
	private Uri selectedRingtone;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_selectable_list, container, false);

			setRingtoneList();
		}

		Selectable.selectOnlyPosition(ringtones, Ringtone.indexOf(ringtones, selectedRingtone));
		((RecyclerView) rootView.findViewById(R.id.selectableList)).scrollToPosition(Selectable.getSelectedPosition(ringtones));

		return rootView;
	}

	private void setRingtoneList() {
		RecyclerView ringtoneList = rootView.findViewById(R.id.selectableList);
		RingtoneListAdapter ringtoneListAdapter = new RingtoneListAdapter(ringtones = getRingtone());
		ringtoneList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
		ringtoneList.setAdapter(ringtoneListAdapter);
		ringtoneListAdapter.setOnItemClickListener(new RingtoneListAdapter.MyClickListener() {
			@Override public void onRingtoneClick(int position) {
				if(!ringtones.get(position).getUri().equals(Uri.EMPTY))
					onRingtonePlay(position);
				Selectable.selectOnlyPosition(ringtones, position);
				ringtoneListAdapter.notifyDataSetChanged();
				onRingtonePickListener.onRingtonePick(ringtones.get(position));
			}

			@Override
			public void onRingtonePlay(int position) {
				if(playingRingtone != null)
					playingRingtone.stop();
				playingRingtone = RingtoneManager.getRingtone(getContext(), ringtones.get(position).getUri());
				playingRingtone.play();
			}
		});

		int index = Ringtone.indexOf(ringtones, selectedRingtone);
		Selectable.selectOnlyPosition(ringtones, index >= 0 && index < ringtones.size() ? index : 0);
	}

	private ArrayList<Ringtone> getRingtone() {
		RingtoneManager manager = new RingtoneManager(getActivity());
		manager.setType(RingtoneManager.TYPE_NOTIFICATION);
		Cursor cursor = manager.getCursor();
		ArrayList<Ringtone> ringtones = new ArrayList<>();
		while (cursor.moveToNext()) {
			String notificationTitle = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);
			String notificationUri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);
			Uri uri = Uri.parse(notificationUri + "/" + cursor.getString(RingtoneManager.ID_COLUMN_INDEX));
			ringtones.add(new Ringtone(notificationTitle, uri));
		}

		ringtones.add(0, new Ringtone(getContext().getResources().getString(R.string.silent), Uri.EMPTY));

		return ringtones;
	}

	public RingtoneListFragment setOnRingtonePickListener(OnRingtonePickListener onRingtonePickListener) {
		this.onRingtonePickListener = onRingtonePickListener;
		return this;
	}

	public RingtoneListFragment setRingtoneUri(Uri selectedRingtone) {
		this.selectedRingtone = selectedRingtone;
		return this;
	}

	public interface OnRingtonePickListener {
		void onRingtonePick(@Nullable Ringtone ringtone);
	}
}
