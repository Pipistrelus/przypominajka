package com.infinitemind.przypominajka.fragments;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.ListItemAdapter;
import com.infinitemind.przypominajka.alarms.Alarms;
import com.infinitemind.przypominajka.interfaces.DataRunnable;
import com.infinitemind.przypominajka.interfaces.OnTextChangedListener;
import com.infinitemind.przypominajka.models.Alarm;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Item;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.notifications.Notifications;
import com.infinitemind.przypominajka.utils.BottomPaddingItemDecoration;
import com.infinitemind.przypominajka.utils.Constants;
import com.infinitemind.przypominajka.utils.RepeatFormatter;
import com.infinitemind.przypominajka.utils.MovableItemTouchHelperCallback;
import com.infinitemind.przypominajka.utils.UnscrollaleLayoutManager;
import com.infinitemind.przypominajka.utils.Utils;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddNoteFragment extends Fragment implements ColorAddonFragment.OnColorPickListener, AlarmAddonFragment.OnAlarmSetListener,
		LockAddonFragment.OnLockChangeListener, CategoryListFragment.OnCategoryPickListener, SilentAddonFragment.OnSilentChangeListener,
		LocationAddonFragment.OnLocationSetListener {

	private DataRunnable<Fragment> onAddonClickListener;
	private OnButtonClicksListener onButtonClicksListener;
	private CategoryListFragment categoryAddonFragment;
	private ColorAddonFragment colorAddonFragment;
	private AlarmAddonFragment alarmAddonFragment;
	private LocationAddonFragment locationAddonFragment;
	private LockAddonFragment lockAddonFragment;
	private SilentAddonFragment silentAddonFragment;
	private boolean colorManuallySet;
	private boolean manuallyEdited;
	private ViewGroup rootView;
	private UUID categoryID;
	private UUID noteID;
	private Note note;
	private Context context;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		context = getContext();
		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_add_note, container, false);
			Note note = Data.getItemById(Note.class, noteID);
			this.note = (noteID == null || note == null ? new Note() : new Note(note));
			if(categoryID != null)
				this.note.setCategoryID(categoryID);
			displayAddons();
			handleClicks();
			setOnEditContentListener();
//			setItemsList();
			refreshData();
		}

		View editContent = rootView.findViewById(R.id.editContent);
		editContent.requestFocus();
		Utils.showKeyboard(context, editContent);

		return rootView;
	}

	private void displayAddons() {
		categoryAddonFragment = new CategoryListFragment().setOnCategoryPickListener(this).setCategoryID(note.getCategoryID());
		colorAddonFragment = new ColorAddonFragment().setOnColorPickListener(this).setColor(note.getColor());
		alarmAddonFragment = new AlarmAddonFragment().setOnAlarmSetListener(this).setAlarm(note.getAlarm());
		locationAddonFragment = new LocationAddonFragment().setOnLocationSetListener(this);
		lockAddonFragment = new LockAddonFragment().setOnLockChangeListener(this).setLock(note.getLockState());
		silentAddonFragment = new SilentAddonFragment().setOnLockChangeListener(this).setSilent(note.isSilent());
		addAddon(colorAddonFragment);
		addAddon(alarmAddonFragment);
//		addAddon(locationAddonFragment);
		addAddon(lockAddonFragment);
		addAddon(silentAddonFragment);
		rootView.findViewById(R.id.categoryIcon).setOnClickListener(view -> onAddonClickListener.run(categoryAddonFragment));
	}

	private void addAddon(AddonFragment addon) {
		int dp30 = Utils.dpToPx(context, 30);
		ImageView imageView = new ImageView(context);
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dp30, dp30);
		params.rightMargin = dp30 / 4;
		imageView.setLayoutParams(params);
		imageView.setColorFilter(getResources().getColor(R.color.color1));
		imageView.setImageResource(addon.getIcon());
		imageView.setClickable(true);
		TypedValue outValue = new TypedValue();
		context.getTheme().resolveAttribute(android.R.attr.selectableItemBackgroundBorderless, outValue, true);
		imageView.setBackgroundResource(outValue.resourceId);
		imageView.setOnClickListener(view -> {
			if(addon.hasStates()) {
				addon.changeState();
				imageView.setImageResource(addon.getIcon());
			} else if(onAddonClickListener != null)
				onAddonClickListener.run((Fragment) addon);
			animateAddon(view);
		});

		((LinearLayout) rootView.findViewById(R.id.addonsContainer)).addView(imageView);
	}

	private void animateAddon(View addon) {
		ObjectAnimator rotation = ObjectAnimator.ofFloat(addon, "rotation", 0f, -20f, 20f, 0f);
		rotation.setDuration(Constants.DURATION).setInterpolator(new AccelerateDecelerateInterpolator());
		rotation.start();
	}

	private void setOnEditContentListener() {
		((AppCompatEditText) rootView.findViewById(R.id.editContent)).addTextChangedListener(
				new OnTextChangedListener(editable -> manuallyEdited = manuallyEdited || !editable.toString().equals(note.getContent())));

		rootView.findViewById(R.id.background).setOnClickListener(view -> Utils.showKeyboard(context, rootView.findViewById(R.id.editContent)));
		rootView.findViewById(R.id.editSpace).setOnClickListener(view -> Utils.showKeyboard(context, rootView.findViewById(R.id.editContent)));
	}

	private void setItemsList() {
		RecyclerView itemsList = rootView.findViewById(R.id.itemsList);
		itemsList.setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.editSpace).setVisibility(View.GONE);
		rootView.findViewById(R.id.editContent).setVisibility(View.GONE);

		ArrayList<Item> list = new ArrayList<>();
		list.add(new Item(""));
		ListItemAdapter listItemAdapter = new ListItemAdapter(list, note.getColor());
		ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new MovableItemTouchHelperCallback(listItemAdapter));
		listItemAdapter.setOnItemClickListener(new ListItemAdapter.MyClickListener() {
			@Override
			public void onRemoveItemClick(int position) {
				list.remove(position);
				new Handler().post(listItemAdapter::notifyDataSetChanged);
			}

			@Override
			public void onStartDrag(ListItemAdapter.DataObjectHolder holder) {
				itemTouchHelper.startDrag(holder);
			}

			@Override
			public void onItemClick(int position) {
				list.add(new Item(""));
				new Handler().post(listItemAdapter::notifyDataSetChanged);
			}
		});
		itemsList.setLayoutManager(new UnscrollaleLayoutManager(context, LinearLayoutManager.VERTICAL, false));
		itemsList.setAdapter(listItemAdapter);
		itemsList.addItemDecoration(new BottomPaddingItemDecoration(Utils.dpToPx(context, 50)));
		itemTouchHelper.attachToRecyclerView(itemsList);
	}

	private void handleClicks() {
		rootView.findViewById(R.id.cancelAlarmButton).setOnClickListener(view -> {
			manuallyEdited = true;
			note.setAlarmSet(false);
			rootView.findViewById(R.id.alarmInfoContainer).setVisibility(View.GONE);
			TransitionManager.beginDelayedTransition(rootView.findViewById(R.id.background), new AutoTransition());
		});

		rootView.findViewById(R.id.doneButton).setOnClickListener(view -> {
			note.setContent(((AppCompatEditText) rootView.findViewById(R.id.editContent)).getText().toString());
			note.setChecked(false);
			if(noteID != null) {
				int index = Data.indexOfItemById(Note.class, noteID);
				if(index != -1) {
					Data.allNotes.set(index, note);
					if(note.isAlarmSet() && note.getAlarm() != null) {
						Notifications.hideNotification(context, note.getId().hashCode());
						Alarms.cancelAlarm(context, note);
						Alarms.setAlarm(context, note, true);
					} else note.setAlarm(new Alarm());
				}
			} else if(note != null && !note.getContent().isEmpty()) {
				Data.allNotes.add(note);
				if(note.isAlarmSet() && note.getAlarm() != null) {
					Alarms.cancelAlarm(context, note);
					Alarms.setAlarm(context, note, true);
				} else note.setAlarm(new Alarm());
			}
			Data.saveData(context);
			onButtonClicksListener.doneButtonClick(true);
		});

		rootView.findViewById(R.id.deleteButton).setOnClickListener(view -> {
			if(note != null && noteID != null || manuallyEdited)
				showDiscardDialog(noteID != null, null);
			else onButtonClicksListener.deleteButtonClick(true);
		});
	}

	private void showDiscardDialog(boolean delete, DataRunnable<Boolean> callback) {
		Utils.makeDialog(getActivity(), R.layout.dialog_confirmation, dialog -> {
			((AppCompatTextView) dialog.findViewById(R.id.text)).setText(getResources().getString(
					delete ? R.string.delete_note_confirmation : R.string.discard_changes_confirmation));
			dialog.findViewById(R.id.noButton).setOnClickListener(view1 -> {
				if(callback != null)
					callback.run(false);
				dialog.dismiss();
			});
			dialog.findViewById(R.id.yesButton).setOnClickListener(view1 -> {
				if(delete) {
					int index = Data.indexOfItemById(Data.allNotes, noteID);
					if(index >= 0 && index < Data.allNotes.size()) {
						Data.allNotes.remove(index);
						Data.saveData(context);
						Alarms.cancelAlarm(context, note);
						Notifications.hideNotification(context, note.getId().hashCode());
					}
				}
				onButtonClicksListener.deleteButtonClick(true);
				if(callback != null)
					callback.run(true);
				dialog.dismiss();
			});
		});
	}

	private void refreshData() {
		((AppCompatEditText) rootView.findViewById(R.id.editContent)).setText(note.getContent());
		int color = note.getColor();
		Alarm alarm = note.getAlarm();
		Category category = Data.getItemById(Category.class, note.getCategoryID());
		if(color != 0) onColorPick(color);
		if(alarm != null && note.isAlarmSet()) onAlarmSet(alarm);
		if(category == null && !Data.allCategories.isEmpty())
			category = Data.allCategories.get(new Random().nextInt(Data.allCategories.size()));
		else if(Data.allCategories.isEmpty())
			category = new Category(getResources().getString(R.string.add_a_category), getResources().getColor(R.color.colorAccent3), getResources().getString(R.string.ic_plus));
		onCategoryPick(category);

		manuallyEdited = false;
	}


	public AddNoteFragment setOnAddonClickListener(DataRunnable<Fragment> onAddonClickListener) {
		this.onAddonClickListener = onAddonClickListener;
		return this;
	}

	public AddNoteFragment setOnButtonClicksListener(OnButtonClicksListener onButtonClicksListener) {
		this.onButtonClicksListener = onButtonClicksListener;
		return this;
	}

	public AddNoteFragment setNoteID(UUID noteID) {
		this.noteID = noteID;
		return this;
	}

	public AddNoteFragment setCategoryID(UUID categoryID) {
		this.categoryID = categoryID;
		return this;
	}

	public void onBackPressed(Runnable callback) {
		if(manuallyEdited)
			showDiscardDialog(false, confirmed -> {
				if(confirmed) callback.run();
			});
		else callback.run();
	}

	@Override
	public void onColorPick(int color) {
		manuallyEdited = true;
		colorManuallySet = true;
		note.setColor(color);
		colorAddonFragment.setColor(color);
		((CircleImageView) rootView.findViewById(R.id.categoryIconBorder)).setBorderColor(color);
		((AppCompatTextView) rootView.findViewById(R.id.categoryIcon)).setTextColor(color);
		((AppCompatTextView) rootView.findViewById(R.id.textHashCategory)).setTextColor(color);
		rootView.findViewById(R.id.divider).setBackgroundColor(color);
		onAddonClickListener.run(this);
		Log.d("LOG!", "colorPick: " + manuallyEdited);
	}

	@Override
	public void onColorRemove(int color) {
		if(note.getColor() == color) {
			Category category = Data.getItemById(Category.class, note.getCategoryID());
			if(category == null)
				onColorPick(Data.allColors.get(new Random().nextInt(Data.allColors.size())).getColor());
			else onColorPick(category.getColor());
			manuallyEdited = false;
		}
		Log.d("LOG!", "colorRemove: " + manuallyEdited);
	}

	@Override
	public void onAlarmSet(Alarm alarm) {
		manuallyEdited = true;
		note.setAlarm(alarm);
		note.setAlarmSet(true);
		alarmAddonFragment.setAlarm(alarm);
		String repeat = RepeatFormatter.format(rootView.getContext(), alarm);
		((AppCompatTextView) rootView.findViewById(R.id.textDate)).setText(Utils.dateFormat1.format(alarm.getDate().getTime()));
		((AppCompatTextView) rootView.findViewById(R.id.textRepeat)).setText(repeat);
		rootView.findViewById(R.id.textRepeat).setVisibility(repeat.isEmpty() ? View.GONE : View.VISIBLE);
		rootView.findViewById(R.id.alarmInfoContainer).setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.cancelAlarmButton).setVisibility(View.VISIBLE);
		rootView.findViewById(R.id.textDate).setVisibility(View.VISIBLE);
		onAddonClickListener.run(this);
		Log.d("LOG!", "alarmSet: " + manuallyEdited);
	}

	@Override
	public void onLocationSet() {

	}

	@Override
	public void onLocationDisabled() {
		onAddonClickListener.run(this);
	}

	@Override
	public void onCategoryPick(Category category) {
		manuallyEdited = true;
		category.setSelected(true);
		note.setCategoryID(category.getId());
		categoryAddonFragment.setCategoryID(category.getId());
		((AppCompatTextView) rootView.findViewById(R.id.categoryIcon)).setText(category.getIcon());
		((AppCompatTextView) rootView.findViewById(R.id.textHashCategory)).setText(String.format("#%s", category.getName().toLowerCase().replaceAll(" ", "")));
		if(!colorManuallySet) {
			onColorPick(category.getColor());
			colorAddonFragment.setColor(category.getColor());
			colorManuallySet = false;
		}
		onAddonClickListener.run(this);
		Log.d("LOG!", "categoryPick: " + manuallyEdited);
	}

	@Override
	public void onCategoryRemove(Category category) {
		if(note.getCategoryID().equals(category.getId()) && !Data.allCategories.isEmpty()) {
			onCategoryPick(Data.allCategories.get(new Random().nextInt(Data.allCategories.size())));
			manuallyEdited = false;
		}
		Log.d("LOG!", "categoryRemove: " + manuallyEdited);
	}

	@Override
	public void onLockChange(@LockAddonFragment.LockState int state) {
		manuallyEdited = true;
		note.setLockState(state);
		lockAddonFragment.setLock(state);
		Log.d("LOG!", "lockChange: " + manuallyEdited);
	}

	@Override
	public void onSilentChange(boolean silent) {
		manuallyEdited = true;
		note.setSilent(silent);
		silentAddonFragment.setSilent(silent);
		Log.d("LOG!", "silentChange: " + manuallyEdited);
	}

	public interface OnButtonClicksListener {
		void doneButtonClick(boolean force);
		void deleteButtonClick(boolean force);
	}
}