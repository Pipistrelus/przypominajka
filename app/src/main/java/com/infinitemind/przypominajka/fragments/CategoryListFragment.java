package com.infinitemind.przypominajka.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.CategoriesListAdapter;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Selectable;
import com.infinitemind.przypominajka.utils.Utils;

import java.util.ArrayList;
import java.util.UUID;

public class CategoryListFragment extends Fragment {

	private ViewGroup rootView;
	private ArrayList<Category> categories;
	private CategoriesListAdapter categoriesListAdapter;
	private OnCategoryPickListener onCategoryPickListener;
	private Category category;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

		if(rootView == null) {
			rootView = (ViewGroup) inflater.inflate(R.layout.fragment_selectable_list, container, false);
			setCategoriesList();
			Selectable.selectOnlyPosition(categories, categories.indexOf(category));
		}
		categoriesListAdapter.notifyDataSetChanged();

		return rootView;
	}

	private void setCategoriesList() {
		RecyclerView categoriesList = rootView.findViewById(R.id.selectableList);
		categoriesList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
		categoriesList.setAdapter(categoriesListAdapter = new CategoriesListAdapter(categories = new ArrayList<>(Data.allCategories)));
		categoriesListAdapter.setOnItemClickListener(new CategoriesListAdapter.MyClickListener() {
			@Override public void onItemClick(int position) {
				if(position < categories.size() - 1 && onCategoryPickListener != null) {
					Selectable.selectOnlyPosition(categories, position);
					categoriesListAdapter.notifyDataSetChanged();
					onCategoryPickListener.onCategoryPick(categories.get(position));
				} else if(position == categories.size() - 1)
					Category.showCategoryDialog(getActivity(), null, category -> {
						Selectable.toggleAll(categories, false);
						category.setSelected(true);
						categories.add(categories.size() - 1, category);
						categoriesListAdapter.notifyDataSetChanged();
						onCategoryPickListener.onCategoryPick(category);
					});
			}
			@Override public void onItemLongClick(int position) {
				if(position != categories.size() - 1 && onCategoryPickListener != null)
					showDeleteCategoryDialog(position);
			}
		});

		categories.add(new Category(getResources().getString(R.string.add_a_category), getResources().getColor(R.color.colorAccent3), getResources().getString(R.string.ic_plus)));
		categoriesListAdapter.notifyDataSetChanged();
	}

	private void showDeleteCategoryDialog(int position) {
		Utils.makeDialog(getActivity(), R.layout.dialog_confirmation, dialog -> {
			((AppCompatTextView) dialog.findViewById(R.id.text)).setText(getContext().getResources().getString(R.string.delete_category_confirmation));

			dialog.findViewById(R.id.yesButton).setOnClickListener(view -> {
				Category category = Data.allCategories.remove(position);
				categories.remove(position);
				categoriesListAdapter.notifyItemRemoved(position);
				Data.saveCategories(getContext());
				onCategoryPickListener.onCategoryRemove(category);
				dialog.dismiss();
			});

			dialog.findViewById(R.id.noButton).setOnClickListener(view -> dialog.dismiss());
		});
	}

	public CategoryListFragment setOnCategoryPickListener(OnCategoryPickListener onCategoryPickListener) {
		this.onCategoryPickListener = onCategoryPickListener;
		return this;
	}

	public CategoryListFragment setCategoryID(UUID categoryID) {
		this.category = Data.getItemById(Category.class, categoryID);
		return this;
	}

	interface OnCategoryPickListener {
		void onCategoryPick(Category category);
		void onCategoryRemove(Category category);
	}
}
