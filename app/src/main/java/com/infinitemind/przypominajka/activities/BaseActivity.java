package com.infinitemind.przypominajka.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.models.Data;

public abstract class BaseActivity extends Activity {

	protected ViewGroup rootView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rootView = (ViewGroup) getWindow().getDecorView().getRootView();
		Data.loadData(getApplicationContext());
	}
}
