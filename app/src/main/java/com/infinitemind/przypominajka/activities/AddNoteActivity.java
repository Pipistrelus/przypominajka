package com.infinitemind.przypominajka.activities;

import android.animation.Animator;
import android.app.KeyguardManager;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.WindowManager;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.alarms.Alarms;
import com.infinitemind.przypominajka.fragments.AddNoteFragment;
import com.infinitemind.przypominajka.fragments.AlarmAddonFragment;
import com.infinitemind.przypominajka.models.Alarm;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.notifications.Notifications;
import com.infinitemind.przypominajka.utils.Constants;
import com.infinitemind.przypominajka.utils.Utils;

import java.util.UUID;

public class AddNoteActivity extends BaseFragmentActivity implements AddNoteFragment.OnButtonClicksListener, AlarmAddonFragment.OnAlarmSetListener {

	private AddNoteFragment addNoteFragment;
	private boolean mainContainer;
	private boolean replaceAnimation;
	private UUID categoryID;
	private UUID noteID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_note);

		enterAnimation();
		getData(() -> setAddonFragment(addNoteFragment = new AddNoteFragment().setOnAddonClickListener(this::setAddonFragment)
				.setOnButtonClicksListener(this).setNoteID(noteID).setCategoryID(categoryID)));
	}

	private void getData(Runnable callback) {
		Intent intent = getIntent();
		String noteID = intent.getStringExtra("id");
		if(noteID != null && !noteID.isEmpty())
			this.noteID = UUID.fromString(noteID);
		String categoryID = intent.getStringExtra("categoryID");
		if(categoryID != null && !categoryID.isEmpty())
			this.categoryID = UUID.fromString(categoryID);
		if(intent.getBooleanExtra("getDate", false))
			setAddonFragment(new AlarmAddonFragment().setOnAlarmSetListener(this).getDate());
		else callback.run();
	}

	private void enterAnimation() {
		findViewById(R.id.mainContainer).post(() -> {
			Point screen = Utils.getScreenSize(getApplicationContext());
			Animator anim = ViewAnimationUtils.createCircularReveal(
					findViewById(R.id.mainContainer),
					screen.x / 2, screen.y / 2,
					0, (float) Math.hypot(screen.x, screen.y) / 2);
			anim.setDuration(Constants.DURATION);
			anim.start();
		});
	}

	public void setAddonFragment(Fragment addonFragment) {
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.setCustomAnimations(replaceAnimation ? R.anim.addon_in : R.anim.slide_up_fade_in, R.anim.addon_out);
		fragmentTransaction.replace(R.id.fragmentsContainer, addonFragment).commit();
		mainContainer = addonFragment instanceof AddNoteFragment;
		replaceAnimation = true;
		Utils.hideKeyboard(this);
	}

	private void close() {
		finish();
		overridePendingTransition(0, R.anim.fade_out);
	}

	@Override
	public void onBackPressed() {
		if(mainContainer) addNoteFragment.onBackPressed(this::close);
		else if(addNoteFragment != null) setAddonFragment(addNoteFragment);
		else close();
	}

	@Override
	public void doneButtonClick(boolean force) {
		if(force) close();
		else onBackPressed();
	}

	@Override
	public void deleteButtonClick(boolean force) {
		if(force) close();
		else onBackPressed();
	}

	@Override //Only for getting date request
	public void onAlarmSet(Alarm date) {
		if(noteID != null) {
			Data.loadNotes(getApplicationContext());
			Note temp = Data.getItemById(Note.class, noteID);
			if(temp != null) {
				Note note = new Note(temp);
				note.setAlarm(date);
				Notifications.hideNotification(getApplicationContext(), noteID.hashCode());
				Alarms.setAlarm(getApplicationContext(), note, true);
				close();
			}
		}
	}
}
