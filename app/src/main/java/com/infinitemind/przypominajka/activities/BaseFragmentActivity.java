package com.infinitemind.przypominajka.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.models.Data;

public abstract class BaseFragmentActivity extends FragmentActivity {

	protected ViewGroup rootView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rootView = (ViewGroup) getWindow().getDecorView().getRootView();
		Data.loadData(getApplicationContext());
	}
}
