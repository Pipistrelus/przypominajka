package com.infinitemind.przypominajka.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.fragments.ListFragment;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.notifications.Notifications;

import java.util.UUID;

public class MainActivity extends BaseFragmentActivity implements ListFragment.OnNoteClickListener {

	private ListFragment listFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if(Data.quickAddEnabled)
			Notifications.showQuickAddNotification(getApplicationContext());

		setFragments();
		restoreSettings();
	}

	private void setFragments() {
		listFragment = new ListFragment().setOnNoteClickListener(this);
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.fragmentsContainer, listFragment).commit();
	}

	private void switchFragments(Fragment fragment) {
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
		fragmentTransaction.replace(R.id.fragmentsContainer, fragment).commit();
	}

	private void restoreSettings() {
		((ImageView) findViewById(R.id.buttonMode)).setImageResource(Data.groupType.getIcon());
	}

	public void clickChangeMode(View view) {
		/*int index = (Data.groupType.ordinal() + 1) % ListFragment.GroupType.values().length;
		Data.groupType = ListFragment.GroupType.values()[index];
		Data.saveSettings(getApplicationContext());
		restoreSettings();

		if(listFragment != null) {
			listFragment.selectedSection = null;
			listFragment.wholeListShowed = true;
			listFragment.refreshList();
		}*/
//		switchFragments(eventFragment);
	}

	public void clickSettings(View view) {
		startActivity(new Intent(this, SettingsActivity.class));
		overridePendingTransition(R.anim.fade_in, 0);
	}

	public void clickAdd(View view) {
		startActivity(new Intent(this, AddNoteActivity.class));
		overridePendingTransition(0, 0);
	}

	@Override
	public void onNoteClick(UUID noteID) {
		startActivity(new Intent(this, AddNoteActivity.class).putExtra("id", noteID.toString()));
		overridePendingTransition(0, 0);
	}

	@Override
	public void onBackPressed() {
		if(!(listFragment.wholeListShowed = !listFragment.wholeListShowed))
			super.onBackPressed();
		else {
			listFragment.selectedSection = null;
			listFragment.refreshList();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(listFragment != null)
			listFragment.refreshList();
		restoreSettings();
	}
}
