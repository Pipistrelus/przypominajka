package com.infinitemind.przypominajka.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.fragments.SettingsListFragment;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Ringtone;
import com.infinitemind.przypominajka.models.Selectable;
import com.infinitemind.przypominajka.notifications.Notifications;
import com.infinitemind.przypominajka.settings.FragmentItem;
import com.infinitemind.przypominajka.settings.SwitchItem;
import com.infinitemind.przypominajka.utils.Constants;

import java.util.ArrayList;

public class SettingsActivity extends BaseFragmentActivity implements SettingsListFragment.OnChangeListener {

	private SettingsListFragment settingsListFragment;
	private GoogleSignInClient mGoogleSignInClient;
	private GoogleSignInAccount signedInAccount;
	private boolean mainContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		init();
		setFragments();
	}

	private void init() {
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestIdToken(getResources().getString(R.string.web_client_token_id))
				.requestProfile()
				.build();
		mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
		signedInAccount = GoogleSignIn.getLastSignedInAccount(this);
	}

	private void setFragments() {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.add(R.id.fragmentsContainer, settingsListFragment = new SettingsListFragment().setOnChangeListener(this).setLastLoggedAccount(signedInAccount)).commit();
		mainContainer = true;
	}

	public void clickDone(View view) {
		onBackPressed();
	}

	public void clickDismiss(View view) {
		close();
	}

	private void close() {
		finish();
		overridePendingTransition(0, R.anim.fade_out);
	}

	@Override
	public void onBackPressed() {
		if(mainContainer) {
			close();
		} else onFragmentChange(settingsListFragment);
	}

	@Override
	public void onFragmentChange(Fragment fragment) {
		mainContainer = fragment instanceof SettingsListFragment;
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(mainContainer ? R.anim.slide_right_in : R.anim.slide_left_in, mainContainer ? R.anim.slide_right_out : R.anim.slide_left_out);
		transaction.replace(R.id.fragmentsContainer, fragment).commit();

		((AppCompatTextView) findViewById(R.id.doneButtonText)).setText(getResources().getString(mainContainer ? R.string.done : R.string.back));
		((CardView) findViewById(R.id.doneButton)).setCardBackgroundColor(getResources().getColor(mainContainer ? R.color.colorAccent1 : R.color.colorText3));
	}

	@Override
	public void onRingtonePick(Ringtone ringtone) {
		Data.selectedRingtone = ringtone;
		Data.saveSettings(getApplicationContext());
	}

	@Override
	public void onVibrationChange(boolean enabled) {
		Data.vibrationEnabled = enabled;
		Data.saveSettings(getApplicationContext());
	}

	@Override
	public void onQuickAddChange(boolean enabled) {
		Data.quickAddEnabled = enabled;
		Data.saveSettings(getApplicationContext());
		if(enabled)
			Notifications.showQuickAddNotification(getApplicationContext());
		else Notifications.hideQuickAddNotification(getApplicationContext());
	}

	@Override
	public void onQuickCategoryChange(ArrayList<Category> categories) {
		Data.quickCategories = Selectable.getSelectedArray(categories);
		Data.saveSettings(getApplicationContext());
		Notifications.showQuickAddNotification(getApplicationContext());
	}

	@Override
	public void onSignInClick() {
		GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
		if(account == null)
			startActivityForResult(mGoogleSignInClient.getSignInIntent(), Constants.SIGN_IN_REQUEST_CODE);
		else mGoogleSignInClient.signOut().addOnCompleteListener(task -> {
			FirebaseAuth.getInstance().signOut();
			settingsListFragment.signInItem.setTitle(getResources().getString(R.string.sign_in)).refreshTitle();
			settingsListFragment.signInItem.setSubtitle(getResources().getString(R.string.tap_to_sign_in)).refreshSubtitle();
			settingsListFragment.synchronizeItem.toggle(false);
		});
	}

	@Override
	public void onSynchronizeClick() {
		Data.synchronize(getApplicationContext(), signedInAccount, () -> {
			((SwitchItem) settingsListFragment.vibrationItem).setChecked(Data.vibrationEnabled);
			((SwitchItem) settingsListFragment.quickAddItem).setChecked(Data.quickAddEnabled);
			settingsListFragment.quickCategoryItem.toggle(Data.quickAddEnabled);
		});
		onFragmentChange(settingsListFragment);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if(requestCode == Constants.SIGN_IN_REQUEST_CODE) {
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			try {
				signedInAccount = task.getResult(ApiException.class);
				settingsListFragment.signInItem.setSubtitle(signedInAccount.getDisplayName()).refresh();
				settingsListFragment.synchronizeItem.toggle(true);
			} catch (ApiException ignored) { }
		}
	}
}
