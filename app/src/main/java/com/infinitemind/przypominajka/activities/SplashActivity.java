package com.infinitemind.przypominajka.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		Fabric.with(this, new Crashlytics());

		startActivity(new Intent(this, MainActivity.class));
		finish();
	}
}
