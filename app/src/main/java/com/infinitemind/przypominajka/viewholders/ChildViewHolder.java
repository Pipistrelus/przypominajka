package com.infinitemind.przypominajka.viewholders;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;

import com.infinitemind.przypominajka.R;

public class ChildViewHolder extends RecyclerView.ViewHolder {

	public AppCompatTextView textContent, textDate, textTime;
	public CheckBox checkDone;
	public CardView cardView;
	public View divider;

	public ChildViewHolder(View itemView) {
		super(itemView);
		textContent = itemView.findViewById(R.id.textContent);
		textDate = itemView.findViewById(R.id.textDate);
		textTime = itemView.findViewById(R.id.textTime);
		checkDone = itemView.findViewById(R.id.checkDone);
		cardView = itemView.findViewById(R.id.cardView);
		divider = itemView.findViewById(R.id.divider);
	}
}
