package com.infinitemind.przypominajka.viewholders;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;

public class SectionViewHolder extends RecyclerView.ViewHolder {

	public AppCompatTextView title, icon;
	public ViewGroup background;

	public SectionViewHolder(View itemView) {
		super(itemView);
		title = itemView.findViewById(R.id.headerTitle);
		icon = itemView.findViewById(R.id.headerIcon);
		background = itemView.findViewById(R.id.background);
	}
}
