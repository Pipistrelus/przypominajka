package com.infinitemind.przypominajka.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.models.Alarm;
import com.infinitemind.przypominajka.models.Identifiable;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.models.SectionHeader;
import com.infinitemind.przypominajka.utils.Utils;
import com.infinitemind.przypominajka.viewholders.ChildViewHolder;
import com.infinitemind.przypominajka.viewholders.SectionViewHolder;
import com.intrusoft.sectionedrecyclerview.SectionRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class NotesListAdapter extends SectionRecyclerViewAdapter<SectionHeader, Note, SectionViewHolder, ChildViewHolder> {

	private MyClickListener myClickListener;
	private ArrayList<Identifiable> items;
	private Context context;

	public NotesListAdapter(Context context, ArrayList<SectionHeader> sectionItemList) {
		super(context, sectionItemList);
		this.context = context;
		this.items = flattenList(sectionItemList);
		setHasStableIds(true);
	}

	private ArrayList<Identifiable> flattenList(List<SectionHeader> sectionItemList) {
		ArrayList<Identifiable> list = new ArrayList<>();
		for(SectionHeader sectionHeader : sectionItemList) {
			list.add(sectionHeader.getCategory());
			list.addAll(sectionHeader.getChildItems());
		}
		return list;
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getId().hashCode();
	}

	@Override
	public void notifyDataChanged(List<SectionHeader> sectionItemList) {
		items = flattenList(sectionItemList);
		super.notifyDataChanged(sectionItemList);
	}

	public void setOnItemClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	@Override
	public SectionViewHolder onCreateSectionViewHolder(ViewGroup viewGroup, int viewType) {
		View view = LayoutInflater.from(context).inflate(R.layout.header_list_item_view, viewGroup, false);
		return new SectionViewHolder(view);
	}

	@Override
	public ChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup, int viewType) {
		View view = LayoutInflater.from(context).inflate(R.layout.notes_list_item_view, viewGroup, false);
		return new ChildViewHolder(view);
	}

	@Override
	public void onBindSectionViewHolder(SectionViewHolder holder, int sectionPosition, SectionHeader sectionHeader) {
		holder.title.setText(sectionHeader.getCategory().getName());
		holder.icon.setVisibility(sectionHeader.getCategory().getIcon() == null ? View.GONE : View.VISIBLE);
		holder.icon.setText(sectionHeader.getCategory().getIcon());

		holder.background.setOnClickListener(view -> myClickListener.onHeaderClick(sectionPosition));

		if(sectionHeader.isShow()) {
			RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			params.setMargins(0, Utils.dpToPx(context, 10), 0, 0);
			holder.background.setLayoutParams(params);
		}
		else holder.background.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
	}

	@Override
	public void onBindChildViewHolder(ChildViewHolder holder, int sectionPosition, int childPosition, Note note) {
		holder.textContent.setText(note.getContent());
		Alarm alarm = note.getAlarm();
		if(note.isAlarmSet()) {
			holder.textDate.setVisibility(View.VISIBLE);
			holder.textTime.setVisibility(View.VISIBLE);
			holder.textDate.setText(Utils.dateFormat2.format(alarm.getDate().getTime()));
			holder.textTime.setText(Utils.dateFormat3.format(alarm.getDate().getTime()));
		} else {
			holder.textDate.setVisibility(View.GONE);
			holder.textTime.setVisibility(View.GONE);
		}
		holder.checkDone.setChecked(note.isChecked());

		Utils.tint(holder.checkDone, note.getColor());
		holder.divider.setBackgroundColor(note.getColor());

		holder.cardView.setOnClickListener(view -> myClickListener.onItemClick(sectionPosition, childPosition));
		holder.checkDone.setOnCheckedChangeListener((compoundButton, b) -> myClickListener.onItemChecked(sectionPosition, childPosition, b));

		holder.textContent.setPaintFlags(isStrike(holder.textContent, note.isChecked()));
		holder.textTime.setPaintFlags(isStrike(holder.textTime, note.isChecked()));
		holder.textDate.setPaintFlags(isStrike(holder.textDate, note.isChecked()));
	}

	private int isStrike(AppCompatTextView textView, boolean checked) {
		return checked ? (textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG) : textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG;
	}

	public interface MyClickListener {
		void onHeaderClick(int sectionPosition);
		void onItemClick(int sectionPosition, int position);
		void onItemChecked(int sectionPosition, int position, boolean checked);
	}
}
