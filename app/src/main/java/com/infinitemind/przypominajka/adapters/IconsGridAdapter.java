package com.infinitemind.przypominajka.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.models.Color;
import com.infinitemind.przypominajka.models.Icon;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class IconsGridAdapter extends RecyclerView.Adapter<IconsGridAdapter.DataObjectHolder> {
	private MyClickListener myClickListener;
	private ArrayList<Icon> icons;
	private Context context;

	static class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView icon;
		CircleImageView border;

		DataObjectHolder(View itemView) {
			super(itemView);
			icon = itemView.findViewById(R.id.icon);
			border = itemView.findViewById(R.id.border);
		}
	}

	public void setOnItemClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public IconsGridAdapter(ArrayList<Icon> icons) {
		this.icons = icons;
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.selectable_list_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.icon.setTextColor(context.getResources().getColor(R.color.colorText3));
		holder.icon.setText(icons.get(position).getIcon());
		holder.border.setVisibility(icons.get(position).isSelected() ? View.VISIBLE : View.GONE);

		holder.icon.setOnClickListener(view -> myClickListener.onItemClick(position));
	}

	@Override
	public int getItemCount() {
		return icons.size();
	}

	public interface MyClickListener {
		void onItemClick(int position);
	}
}