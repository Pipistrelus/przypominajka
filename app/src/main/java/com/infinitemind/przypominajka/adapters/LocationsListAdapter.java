package com.infinitemind.przypominajka.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.infinitemind.przypominajka.R;

import java.util.ArrayList;

public class LocationsListAdapter extends RecyclerView.Adapter<LocationsListAdapter.DataObjectHolder> {
	private MyClickListener myClickListener;
	private ArrayList<String> locations;

	static class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView title;
		ImageView image;

		DataObjectHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.title);
			image = itemView.findViewById(R.id.image);
		}
	}

	public void setOnItemClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public LocationsListAdapter(ArrayList<String> locations) {
		this.locations = locations;
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.title.setText(locations.get(position));
	}

	@Override
	public int getItemCount() {
		return locations.size();
	}

	public interface MyClickListener {

	}
}
