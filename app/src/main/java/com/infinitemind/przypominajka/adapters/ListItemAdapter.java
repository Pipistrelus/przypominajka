package com.infinitemind.przypominajka.adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.interfaces.OnTextChangedListener;
import com.infinitemind.przypominajka.models.Item;
import com.infinitemind.przypominajka.utils.ItemTouchHelperAdapter;
import com.infinitemind.przypominajka.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.DataObjectHolder> implements ItemTouchHelperAdapter {
	private MyClickListener myClickListener;
	private ArrayList<Item> items;
	private int color;

	@Override
	public void onItemMove(int fromPosition, int toPosition) {
		Collections.swap(items, fromPosition, Math.min(toPosition, Math.max(0, getItemCount() - 2)));
		notifyItemMoved(fromPosition, toPosition);
	}

	public static class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatEditText editContent;
		ImageView buttonRemove, buttonMove;
		CheckBox checkDone;
		ViewGroup background;

		DataObjectHolder(View itemView) {
			super(itemView);
			editContent = itemView.findViewById(R.id.editContent);
			buttonRemove = itemView.findViewById(R.id.buttonRemove);
			buttonMove = itemView.findViewById(R.id.buttonMove);
			checkDone = itemView.findViewById(R.id.checkDone);
			background = itemView.findViewById(R.id.background);
		}
	}

	public void setOnItemClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public ListItemAdapter(ArrayList<Item> items, int color) {
		this.items = items;
		this.color = color;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getId().hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_view, parent, false));
	}

	@Override
	@SuppressLint("ClickableViewAccessibility")
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.editContent.setText(items.get(position).getText());
		holder.editContent.setSelection(Math.max(0, items.get(position).getText().length() - 1));
		Utils.tint(holder.checkDone, color);

		holder.buttonMove.setVisibility(position < getItemCount() - 1 ? View.VISIBLE : View.GONE);
		holder.buttonRemove.setVisibility(position < getItemCount() - 1 ? View.VISIBLE : View.GONE);
		holder.checkDone.setVisibility(position < getItemCount() - 1 ? View.VISIBLE : View.GONE);

		holder.editContent.setOnTouchListener((v, event) -> {
			if(event.getAction() == MotionEvent.ACTION_DOWN && position == getItemCount() - 1) myClickListener.onItemClick(position);
			return false;
		});

		holder.buttonRemove.setOnClickListener(view -> myClickListener.onRemoveItemClick(holder.getAdapterPosition()));

		holder.buttonMove.setOnTouchListener((v, event) -> {
			if(event.getAction() == MotionEvent.ACTION_DOWN)
				myClickListener.onStartDrag(holder);
			return false;
		});
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	public interface MyClickListener {
		void onRemoveItemClick(int position);
		void onStartDrag(DataObjectHolder position);
		void onItemClick(int position);
	}
}