package com.infinitemind.przypominajka.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.models.Category;

import java.util.ArrayList;

public class CategoriesListAdapter extends RecyclerView.Adapter<CategoriesListAdapter.DataObjectHolder> {
	private MyClickListener myClickListener;
	private ArrayList<Category> categories;
	private Context context;

	static class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView name, icon;
		View imageCheck;
		ViewGroup background;

		DataObjectHolder(View itemView) {
			super(itemView);
			name = itemView.findViewById(R.id.name);
			icon = itemView.findViewById(R.id.icon);
			imageCheck = itemView.findViewById(R.id.imageCheck);
			background = itemView.findViewById(R.id.background);
		}
	}

	public void setOnItemClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public CategoriesListAdapter(ArrayList<Category> categories) {
		this.categories = categories;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return categories.get(position).getId().hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.categories_list_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.name.setText(categories.get(position).getName());
		holder.icon.setText(categories.get(position).getIcon());
		holder.icon.setTextColor(categories.get(position).getColor());

		holder.imageCheck.setVisibility(categories.get(position).isSelected() ? View.VISIBLE : View.GONE);

		holder.imageCheck.setOnClickListener(view -> myClickListener.onItemClick(holder.getAdapterPosition()));
		holder.imageCheck.setOnLongClickListener(view -> {
			myClickListener.onItemLongClick(holder.getAdapterPosition());
			return true;
		});
		holder.background.setOnClickListener(view -> myClickListener.onItemClick(holder.getAdapterPosition()));
		holder.background.setOnLongClickListener(view -> {
			myClickListener.onItemLongClick(holder.getAdapterPosition());
			return true;
		});
	}

	@Override
	public int getItemCount() {
		return categories.size();
	}

	public interface MyClickListener {
		void onItemClick(int position);
		void onItemLongClick(int position);
	}
}