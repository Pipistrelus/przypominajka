package com.infinitemind.przypominajka.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.models.Color;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ColorsListAdapter extends RecyclerView.Adapter<ColorsListAdapter.DataObjectHolder> {
	private MyClickListener myClickListener;
	private ArrayList<Color> colors;
	private Context context;

	static class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView icon;
		CircleImageView border;

		DataObjectHolder(View itemView) {
			super(itemView);
			icon = itemView.findViewById(R.id.icon);
			border = itemView.findViewById(R.id.border);
		}
	}

	public void setOnItemClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public ColorsListAdapter(ArrayList<Color> colors) {
		this.colors = colors;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return colors.get(position).getId().hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(context = parent.getContext()).inflate(R.layout.selectable_list_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		if(colors.get(position).getColor() != -1) {
			holder.icon.setTextColor(colors.get(position).getColor());
			holder.icon.setText(context.getResources().getString(R.string.ic_circle));
		} else {
			holder.icon.setTextColor(context.getResources().getColor(R.color.colorAccent3));
			holder.icon.setText(context.getResources().getString(R.string.ic_plus));
		}
		holder.border.setVisibility(colors.get(position).isSelected() ? View.VISIBLE : View.GONE);

		holder.icon.setOnClickListener(view -> myClickListener.onItemClick(position));
		holder.icon.setOnLongClickListener(view -> {
			myClickListener.onItemLongClick(position, colors.get(position));
			return false;
		});
	}

	@Override
	public int getItemCount() {
		return colors.size();
	}

	public interface MyClickListener {
		void onItemClick(int position);
		void onItemLongClick(int position, Color color);
	}
}