package com.infinitemind.przypominajka.adapters;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.models.Ringtone;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RingtoneListAdapter extends RecyclerView.Adapter<RingtoneListAdapter.DataObjectHolder> {
	private MyClickListener myClickListener;
	private ArrayList<Ringtone> ringtones;

	static class DataObjectHolder extends RecyclerView.ViewHolder {
		AppCompatTextView title;
		View imageCheck;
		ImageView buttonPlay;
		ViewGroup background;

		DataObjectHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.title);
			imageCheck = itemView.findViewById(R.id.imageCheck);
			buttonPlay = itemView.findViewById(R.id.buttonPlay);
			background = itemView.findViewById(R.id.background);
		}
	}

	public void setOnItemClickListener(MyClickListener myClickListener) {
		this.myClickListener = myClickListener;
	}

	public RingtoneListAdapter(ArrayList<Ringtone> ringtones) {
		this.ringtones = ringtones;
		setHasStableIds(true);
	}

	@Override
	public long getItemId(int position) {
		return ringtones.get(position).getId().hashCode();
	}

	@NonNull
	@Override
	public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new DataObjectHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.ringtone_list_item_view, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull final DataObjectHolder holder, int position) {
		holder.title.setText(ringtones.get(position).getTitle());
		holder.imageCheck.setVisibility(ringtones.get(position).isSelected() ? View.VISIBLE : View.GONE);
		holder.buttonPlay.setOnClickListener(view -> myClickListener.onRingtonePlay(position));
		holder.background.setOnClickListener(view -> myClickListener.onRingtoneClick(position));

		holder.buttonPlay.setVisibility(ringtones.get(position).getUri().equals(Uri.EMPTY) ? View.GONE : View.VISIBLE);
	}

	@Override
	public int getItemCount() {
		return ringtones.size();
	}

	public interface MyClickListener {
		void onRingtoneClick(int position);
		void onRingtonePlay(int position);
	}
}