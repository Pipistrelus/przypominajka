package com.infinitemind.przypominajka.models;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.adapters.ColorsListAdapter;
import com.infinitemind.przypominajka.adapters.IconsGridAdapter;
import com.infinitemind.przypominajka.interfaces.DataRunnable;
import com.infinitemind.przypominajka.utils.Utils;
import com.skydoves.colorpickerpreference.ColorPickerView;

import java.util.ArrayList;
import java.util.UUID;

public class Category extends Identifiable {
	private String name;
	private int color;
	private String icon;

	Category() {
		this("", 0, "");
	}

	public Category(String name, int color, String icon) {
		this.name = name;
		this.color = color;
		this.icon = icon;
		this.id = UUID.randomUUID();
	}

	public static void showCategoryDialog(Activity activity, @Nullable Category sourceCategory, @Nullable DataRunnable<Category> callback) {
		Utils.makeDialog(activity, R.layout.dialog_add_category, dialog -> {
			if(sourceCategory != null)
				((AppCompatEditText) dialog.findViewById(R.id.editContent)).setText(sourceCategory.getName());

			RecyclerView iconsList = dialog.findViewById(R.id.iconsList);
			RecyclerView colorsList = dialog.findViewById(R.id.colorsList);

			ArrayList<Icon> icons = new ArrayList<>(Data.allIcons);
			ArrayList<Color> colors = new ArrayList<>(Data.allColors);
			Selectable.toggleAll(icons, false);
			Selectable.toggleAll(colors, false);

			setIconsList(activity, sourceCategory, icons, iconsList);
			setColorsList(activity, sourceCategory, colors, colorsList);

			dialog.findViewById(R.id.cancelButton).setOnClickListener(view -> dialog.dismiss());

			dialog.findViewById(R.id.doneButton).setOnClickListener(view -> {
				String name = ((AppCompatEditText) dialog.findViewById(R.id.editContent)).getText().toString();
				Color color = Selectable.getSelected(colors);
				Icon icon = Selectable.getSelected(icons);
				if(!name.isEmpty() && color != null && icon != null) {
					Category category;
					if(sourceCategory != null) {
						sourceCategory.setName(name);
						sourceCategory.setColor(color.getColor());
						sourceCategory.setIcon(icon.getIcon());
						Data.allCategories.set(Data.indexOfItemById(Category.class, sourceCategory.getId()), category = sourceCategory);
					} else {
						category = new Category(name, color.getColor(), icon.getIcon());
						Data.allCategories.add(category);
					}
					Data.saveData(activity);
					if(callback != null)
						callback.run(category);

					dialog.dismiss();
				} else if(name.isEmpty()) {
					Utils.makeDialog(activity, R.layout.dialog_confirmation, confirmationDialog -> {
						((AppCompatTextView) confirmationDialog.findViewById(R.id.title)).setText(activity.getResources().getString(R.string.warning));
						((AppCompatTextView) confirmationDialog.findViewById(R.id.text)).setText(activity.getResources().getString(R.string.empty_field));
						((AppCompatTextView) ((CardView) confirmationDialog.findViewById(R.id.yesButton)).getChildAt(0)).setText(activity.getResources().getString(R.string.ok));
						confirmationDialog.findViewById(R.id.noButton).setVisibility(View.GONE);
						confirmationDialog.findViewById(R.id.yesButton).setOnClickListener(view2 -> confirmationDialog.dismiss());
					});
				}
			});
		});
	}

	private static void setIconsList(Activity activity, @Nullable Category sourceCategory, ArrayList<Icon> icons, RecyclerView iconsList) {
		int index = sourceCategory == null ? Selectable.selectRandom(icons) : Icon.indexOf(icons, sourceCategory.getIcon());
		icons.get(index).setSelected(true);
		IconsGridAdapter iconsGridAdapter = new IconsGridAdapter(icons);
		iconsList.setAdapter(iconsGridAdapter);
		iconsList.setLayoutManager(new GridLayoutManager(activity, 3, LinearLayoutManager.HORIZONTAL, false));
		iconsGridAdapter.setOnItemClickListener(pos -> {
			int temp = Selectable.getSelectedPosition(icons);
			icons.get(temp).setSelected(false);
			icons.get(pos).setSelected(true);
			iconsGridAdapter.notifyItemChanged(temp);
			iconsGridAdapter.notifyItemChanged(pos);
		});
		iconsList.scrollToPosition(index);
	}

	private static void setColorsList(Activity activity, @Nullable Category sourceCategory, ArrayList<Color> colors, RecyclerView colorsList) {
		int index = sourceCategory == null ? Selectable.selectRandom(colors) : Color.indexOf(colors, sourceCategory.getColor());
		if(index >= 0 && index < colors.size())
			colors.get(index).setSelected(true);

		colors.add(new Color(-1));

		ColorsListAdapter colorsListAdapter = new ColorsListAdapter(colors);
		colorsList.setAdapter(colorsListAdapter);
		colorsList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
		colorsListAdapter.setOnItemClickListener(new ColorsListAdapter.MyClickListener() {
			@Override public void onItemClick(int pos) {
				if(pos != colors.size() - 1) {
					Selectable.selectOnlyPosition(colors, pos);
					colorsListAdapter.notifyDataSetChanged();
				} else Color.showAddColorDialog(activity, dialog -> {
					Color color = new Color(((ColorPickerView) dialog.findViewById(R.id.colorPicker)).getColor());
					colors.add(colors.size() - 1, color);
					colorsListAdapter.notifyDataSetChanged();
					Data.allColors.add(color);
					Data.customColorsAmount++;
					Data.saveColors(activity);
					dialog.dismiss();
				});
			}
			@Override public void onItemLongClick(int position, Color color) {
				if(position != colors.size() - 1 && position >= Data.allColors.size() - Data.customColorsAmount)
					Color.showDeleteColorDialog(activity, dialog -> {
						colors.remove(position);
						colorsListAdapter.notifyDataSetChanged();
						Data.allColors.remove(color);
						Data.customColorsAmount--;
						Data.saveColors(activity);
						dialog.dismiss();
					});
			}
		});
		colorsList.scrollToPosition(index);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public boolean equals(Category category) {
		return category.getName().equals(name) &&
				category.getColor() == color &&
				category.getIcon().equals(icon);
	}

	public static Category getCategory(DataSnapshot data) {
		Category category = new Category();
		category.setName(data.child("name").getValue(String.class));
		category.setColor(data.child("color").getValue(Integer.class));
		category.setIcon(data.child("icon").getValue(String.class));
		category.setId(UUID.fromString(data.child("id").getValue(String.class)));
		return category;
	}
}
