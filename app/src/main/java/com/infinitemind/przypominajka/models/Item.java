package com.infinitemind.przypominajka.models;

import java.util.UUID;

public class Item extends Identifiable {

	private String text;

	public Item(String text) {
		this.text = text;
		id = UUID.randomUUID();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
