package com.infinitemind.przypominajka.models;

import java.util.ArrayList;

public class Icon extends Identifiable {
	private String icon;

	public Icon(String icon) {
		this.icon = icon;
	}

	public static ArrayList<Icon> createArrayList(ArrayList<String> stringIcons) {
		ArrayList<Icon> icons = new ArrayList<>();
		for(String s : stringIcons) icons.add(new Icon(s));
		return icons;
	}

	public static int indexOf(ArrayList<Icon> icons, String stringIcon) {
		for(int i = 0; i < icons.size(); i++) {
			Icon icon = icons.get(i);
			if(icon.getIcon().equals(stringIcon))
				return i;
		}
		return -1;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
