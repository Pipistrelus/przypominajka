package com.infinitemind.przypominajka.models;

import java.util.ArrayList;
import java.util.UUID;

public class Identifiable extends Selectable {
	protected UUID id;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public static ArrayList<UUID> getIds(ArrayList<? extends Identifiable> array) {
		ArrayList<UUID> ids = new ArrayList<>();
		for(int i = 0; i < array.size(); i++)
			ids.add(array.get(i).getId());
		return ids;
	}

	public static ArrayList<UUID> getIdsFromStringIds(ArrayList<String> data) {
		ArrayList<UUID> ids = new ArrayList<>();
		for(String p : data)
			if(p != null) ids.add(UUID.fromString(p));
		return ids;
	}

	public static <T extends Identifiable> ArrayList<String> getStringIdsFromData(ArrayList<T> data) {
		ArrayList<String> ids = new ArrayList<>();
		for(T p : data)
			if(p != null) ids.add(p.getId().toString());
		return ids;
	}

	public static ArrayList<String> getStringIdsFromIds(ArrayList<UUID> ids) {
		ArrayList<String> strings = new ArrayList<>();
		for(UUID id : ids)
			strings.add(id.toString());
		return strings;
	}
}
