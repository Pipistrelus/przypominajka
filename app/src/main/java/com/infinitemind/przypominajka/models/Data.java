package com.infinitemind.przypominajka.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.fragments.ListFragment;
import com.infinitemind.przypominajka.fragments.LockAddonFragment;
import com.infinitemind.przypominajka.interfaces.OnDataChangeListener;
import com.infinitemind.przypominajka.utils.Constants;
import com.infinitemind.przypominajka.utils.Utils;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class Data {
	public static ArrayList<Category> allCategories;
	public static ArrayList<Color> allColors;
	public static ArrayList<Note> allNotes;
	public static ArrayList<Icon> allIcons;
	public static ArrayList<Category> quickCategories;
	public static Ringtone selectedRingtone;
	public static boolean vibrationEnabled;
	public static boolean mergeLocally;
	public static boolean mergeConflicts;
	public static boolean cleanUpDatabase;
	public static boolean quickAddEnabled;
	public static ListFragment.GroupType groupType;
	public static int customColorsAmount;

	//////////////////// Loading data ////////////////////

	public static void loadData(Context context) {
		loadNotes(context);
		loadCategories(context);
		loadColors(context);
		loadIcons(context);
		loadSettings(context);
	}

	public static void loadNotes(Context context) {
		if(allNotes == null)
			allNotes = new ArrayList<>();
		else allNotes.clear();
		SharedPreferences sP = context.getSharedPreferences(Constants.NOTES, Context.MODE_MULTI_PROCESS);
		for(int i = 0; i < sP.getInt("size", 0); i++) {
			Note note = new Note();
			Alarm alarm = new Alarm();

			alarm.setRepeat(sP.getBoolean("repeat_" + i, false));
			alarm.setSelectedMode(sP.getBoolean("selectedMode_" + i, false));
			alarm.setUntilMode(sP.getBoolean("untilMode_" + i, false));
			alarm.setEveryAmount(sP.getInt("everyAmount_" + i, 1));
			alarm.setEveryPeriod(sP.getInt("everyPeriod_" + i, 0));
			alarm.setRepeatCount(sP.getInt("repeatCount_" + i, 0));
			try {
				Calendar date = Calendar.getInstance();
				date.setTime(Utils.dateFormat1.parse(sP.getString("date_" + i, Utils.dateFormat1.format(Calendar.getInstance().getTime()))));
				alarm.setDate(date);
				Calendar untilDate = Calendar.getInstance();
				untilDate.setTime(Utils.dateFormat4.parse(sP.getString("untilDate_" + i, Utils.dateFormat4.format(Calendar.getInstance().getTime()))));
				alarm.setUntilDate(untilDate);
			} catch(ParseException e) {
				e.printStackTrace();
			}
			boolean[] selectedDays = new boolean[7];
			for(int j = 0; j < 7; j++)
				selectedDays[j] = sP.getBoolean("selectedDay_" + i + "_" + j, false);
			alarm.setSelectedDays(selectedDays);

			note.setAlarm(alarm);
			note.setAlarmSet(sP.getBoolean("alarmSet_" + i, false));
			note.setSilent(sP.getBoolean("silent_" + i, false));
			note.setContent(sP.getString("content_" + i, ""));
			note.setCategoryID(UUID.fromString(sP.getString("categoryID_" + i, UUID.randomUUID().toString())));
			note.setColor(sP.getInt("color_" + i, context.getResources().getColor(R.color.colorAccent1)));
			note.setLockState(sP.getInt("lockState_" + i, LockAddonFragment.UNLOCKED));
			note.setChecked(sP.getBoolean("checked_" + i, false));
			note.setId(UUID.fromString(sP.getString("id_" + i, UUID.randomUUID().toString())));

			allNotes.add(note);
		}
	}

	public static void loadCategories(Context context) {
		if(allCategories == null)
			allCategories = new ArrayList<>();
		else allCategories.clear();
		SharedPreferences sP = context.getSharedPreferences(Constants.CATEGORIES, Context.MODE_MULTI_PROCESS);
		for(int i = 0; i < sP.getInt("size", 0); i++) {
			Category category = new Category();

			category.setName(sP.getString("name_" + i, ""));
			category.setIcon(sP.getString("icon_" + i, ""));
			category.setColor(sP.getInt("color_" + i, 0));
			category.setId(UUID.fromString(sP.getString("id_" + i, UUID.randomUUID().toString())));

			allCategories.add(category);
		}
	}

	public static void loadColors(Context context) {
		if(allColors == null)
			allColors = new ArrayList<>();
		else allColors.clear();

		ArrayList<Integer> colors = new ArrayList<>();
		Field[] fields = R.color.class.getFields();
		String[] stringNames = new String[fields.length];
		for(int i = 0; i < fields.length; i++) {
			stringNames[i] = fields[i].getName();
			if(stringNames[i].startsWith("color")) {
				int color = context.getResources().getColor(context.getResources().getIdentifier(stringNames[i], "color", context.getPackageName()));
				if(color != android.graphics.Color.WHITE)
					colors.add(color);
			}
		}

		allColors = Color.createArrayList(colors);

		SharedPreferences sP = context.getSharedPreferences(Constants.CUSTOM_COLORS, Context.MODE_MULTI_PROCESS);
		for(int i = 0; i < (customColorsAmount = sP.getInt("size", 0)); i++) {
			Color color = new Color(sP.getInt("color_" + i, 0));
			color.setId(UUID.fromString(sP.getString("id_" + i, UUID.randomUUID().toString())));
			allColors.add(color);
		}
	}

	public static void loadIcons(Context context) {
		if(allIcons == null)
			allIcons = new ArrayList<>();
		else allIcons.clear();

		ArrayList<String> icons = new ArrayList<>();
		Field[] fields = R.string.class.getFields();
		String[] stringNames = new String[fields.length];
		for(int i = 0; i < fields.length; i++) {
			stringNames[i] = fields[i].getName();
			if(stringNames[i].startsWith("ic_"))
				icons.add(context.getResources().getString(context.getResources().getIdentifier(stringNames[i], "string", context.getPackageName())));
		}

		allIcons = Icon.createArrayList(icons);
	}

	public static void loadSettings(Context context) {
		SharedPreferences sP = context.getSharedPreferences(Constants.SETTINGS, Context.MODE_MULTI_PROCESS);
		selectedRingtone = new Ringtone(sP.getString("ringtoneTitle", context.getResources().getString(R.string.silent)), Uri.parse(sP.getString("ringtoneUri", "")));
		vibrationEnabled = sP.getBoolean("vibrationEnabled", true);
		mergeConflicts = sP.getBoolean("mergeConflicts", true);
		mergeLocally = sP.getBoolean("mergeLocally", true);
		cleanUpDatabase = sP.getBoolean("cleanUpDatabase", false);
		quickAddEnabled = sP.getBoolean("quickAddEnabled", true);
		groupType = ListFragment.GroupType.valueOf(sP.getString("groupType", ListFragment.GroupType.CATEGORY.toString()));

		if(quickCategories == null)
			quickCategories = new ArrayList<>();
		else quickCategories.clear();
		for(int i = 0; i < sP.getInt("quickCategories", 0); i++)
			quickCategories.add(getItemById(Category.class, UUID.fromString(sP.getString("quickCategoryId_" + i, UUID.randomUUID().toString()))));
	}

	//////////////////// End loading data ////////////////////




	//////////////////// Saving data ////////////////////

	public static void synchronize(Context context, GoogleSignInAccount signedInAccount, Runnable callback) {
		AtomicInteger count = new AtomicInteger(0);
		Runnable counter = () -> {
			if(count.addAndGet(1) == 4)
				callback.run();
		};
		FirebaseAuth mAuth = FirebaseAuth.getInstance();
		FirebaseUser currentUser = mAuth.getCurrentUser();
		if(currentUser == null && signedInAccount != null) {
			AuthCredential credential = GoogleAuthProvider.getCredential(signedInAccount.getIdToken(), null);
			mAuth.signInWithCredential(credential).addOnCompleteListener(task -> {
				synchronizeNotes(context, mAuth.getCurrentUser(), counter);
				synchronizeCategories(context, mAuth.getCurrentUser(), counter);
				synchronizeSettings(context, mAuth.getCurrentUser(), counter);
				synchronizeColors(context, mAuth.getCurrentUser(), counter);
			});
		}
		if(currentUser != null) {
			synchronizeNotes(context, currentUser, counter);
			synchronizeCategories(context, currentUser, counter);
			synchronizeSettings(context, currentUser, counter);
			synchronizeColors(context, currentUser, counter);
		}
	}


	private static void synchronizeNotes(Context context, FirebaseUser currentUser, Runnable callback) {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference notes = database.getReference(currentUser.getUid()).child(Constants.NOTES);

		if(cleanUpDatabase)
			notes.removeValue((databaseError, databaseReference) -> synchronizeNotesExternally(context, notes, callback));
		else if(!mergeLocally || !mergeConflicts)
			synchronizeNotesExternally(context, notes, () -> synchronizeNotesLocally(context, notes, callback));
		else synchronizeNotesLocally(context, notes, () -> synchronizeNotesExternally(context, notes, callback));
	}

	private static void synchronizeNotesLocally(Context context, DatabaseReference notes, Runnable callback) {
		notes.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			for(DataSnapshot data : dataSnapshot.getChildren()) {
				Note note = Note.getNote(data);
				int index = indexOfItemById(Note.class, note.getId());
				if(index != -1 && !mergeConflicts && !allNotes.get(index).equals(note)) {
					allNotes.get(index).setId(UUID.randomUUID());
					allNotes.add(note);
				} else if(index == -1)
					allNotes.add(note);
				else if(mergeConflicts)
					allNotes.set(index, note);
			}
			saveNotes(context);
			if(callback != null)
				callback.run();
		}));
	}

	private static void synchronizeNotesExternally(Context context, DatabaseReference notes, Runnable callback) {
		if(allNotes.isEmpty() && callback != null) callback.run();
		AtomicInteger done = new AtomicInteger();
		notes.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			for(Note note : allNotes) {
				if(!mergeConflicts && dataSnapshot.hasChild(note.getId().toString())) {
					Note n = Note.getNote(dataSnapshot.child(note.getId().toString()));
					if(!n.equals(note))
						note.setId(UUID.randomUUID());
				}
				done.getAndIncrement();
				uploadNote(notes, note);
				if(done.get() == allNotes.size() && callback != null)
					callback.run();
			}
			saveNotes(context);
		}));
	}

	private static void uploadNote(DatabaseReference notes, Note note) {
		DatabaseReference noteRef = notes.child(note.getId().toString());
		noteRef.child("content").setValue(note.getContent());
		noteRef.child("categoryID").setValue(note.getCategoryID().toString());
		noteRef.child("color").setValue(note.getColor());
		noteRef.child("lockState").setValue(note.getLockState());
		noteRef.child("silent").setValue(note.isSilent());
		noteRef.child("checked").setValue(note.isChecked());
		noteRef.child("alarmSet").setValue(note.isAlarmSet());
		noteRef.child("id").setValue(note.getId().toString());

		ArrayList<Boolean> selectedDays = new ArrayList<>();
		for(int i = 0; i < 7; i++)
			selectedDays.add(note.getAlarm().getSelectedDays()[i]);
		noteRef.child("alarmSelectedDays").setValue(selectedDays);
		noteRef.child("alarmDate").setValue(Utils.dateFormat1.format(note.getAlarm().getDate().getTime()));
		noteRef.child("alarmUntilDate").setValue(Utils.dateFormat4.format(note.getAlarm().getUntilDate().getTime()));
		noteRef.child("alarmRepeat").setValue(note.getAlarm().isRepeat());
		noteRef.child("alarmUntilMode").setValue(note.getAlarm().isUntilMode());
		noteRef.child("alarmSelectedMode").setValue(note.getAlarm().isSelectedMode());
		noteRef.child("alarmRepeatCount").setValue(note.getAlarm().getRepeatCount());
		noteRef.child("alarmEveryAmount").setValue(note.getAlarm().getEveryAmount());
		noteRef.child("alarmEveryPeriod").setValue(note.getAlarm().getEveryPeriod());
	}


	private static void synchronizeCategories(Context context, FirebaseUser currentUser, Runnable callback) {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference categories = database.getReference(currentUser.getUid()).child(Constants.CATEGORIES);

		if(cleanUpDatabase)
			categories.removeValue((databaseError, databaseReference) -> synchronizeCategoriesExternally(context, categories, callback));
		else if(!mergeLocally || !mergeConflicts)
			synchronizeCategoriesExternally(context, categories, () -> synchronizeCategoriesLocally(context, categories, callback));
		else synchronizeCategoriesLocally(context, categories, () -> synchronizeCategoriesExternally(context, categories, callback));
	}

	private static void synchronizeCategoriesLocally(Context context, DatabaseReference categories, Runnable callback) {
		categories.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			for(DataSnapshot data : dataSnapshot.getChildren()) {
				Category category = Category.getCategory(data);
				int index = indexOfItemById(Category.class, category.getId());
				if(index != -1 && !mergeConflicts && !allCategories.get(index).equals(category)) {
					category.setId(UUID.randomUUID());
					allCategories.add(category);
				} else if(index == -1)
					allCategories.add(category);
				else if(mergeConflicts)
					allCategories.set(index, category);
			}
			saveCategories(context);
			if(callback != null)
				callback.run();
		}));
	}

	private static void synchronizeCategoriesExternally(Context context, DatabaseReference categories, Runnable callback) {
		if(allCategories.isEmpty() && callback != null) callback.run();
		AtomicInteger done = new AtomicInteger();
		categories.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			for(Category category : allCategories) {
				if(!mergeConflicts && dataSnapshot.hasChild(category.getId().toString())) {
					Category c = Category.getCategory(dataSnapshot.child(category.getId().toString()));
					if(!c.equals(category))
						category.setId(UUID.randomUUID());
				}
				done.getAndIncrement();
				uploadCategory(categories, category);
				if(done.get() == allCategories.size() && callback != null)
					callback.run();
			}
			saveCategories(context);
		}));
	}

	private static void uploadCategory(DatabaseReference categories, Category category) {
		DatabaseReference categoryRef = categories.child(category.getId().toString());
		categoryRef.child("name").setValue(category.getName());
		categoryRef.child("color").setValue(category.getColor());
		categoryRef.child("icon").setValue(category.getIcon());
		categoryRef.child("id").setValue(category.getId().toString());
	}


	private static void synchronizeSettings(Context context, FirebaseUser currentUser, Runnable callback) {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference settings = database.getReference(currentUser.getUid()).child(Constants.SETTINGS);

		if(cleanUpDatabase)
			settings.removeValue((databaseError, databaseReference) -> synchronizeSettingsExternally(settings, callback));
		else if(!mergeLocally || !mergeConflicts)
			synchronizeSettingsExternally(settings, () -> synchronizeSettingsLocally(context, settings, callback));
		else synchronizeSettingsLocally(context, settings, () -> synchronizeSettingsExternally(settings, callback));
	}

	private static void synchronizeSettingsLocally(Context context, DatabaseReference settings, Runnable callback) {
		settings.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			if(mergeConflicts) {
				if(dataSnapshot.hasChild("vibrationEnabled")) {
					Boolean vibrationEnabled = dataSnapshot.child("vibrationEnabled").getValue(Boolean.class);
					if(vibrationEnabled != null && Data.vibrationEnabled != vibrationEnabled)
						Data.vibrationEnabled = vibrationEnabled;
				}

				if(dataSnapshot.hasChild("quickAddEnabled")) {
					Boolean quickAddEnabled = dataSnapshot.child("quickAddEnabled").getValue(Boolean.class);
					if(quickAddEnabled != null && Data.quickAddEnabled != quickAddEnabled)
						Data.quickAddEnabled = quickAddEnabled;
				}

				if(dataSnapshot.hasChild("quickCategories")) {
					ArrayList<String> strings = dataSnapshot.child("quickCategories").getValue(new GenericTypeIndicator<ArrayList<String>>() {});
					if(strings != null) {
						ArrayList<UUID> quickCategories = Identifiable.getIdsFromStringIds(strings);
						for(UUID id : quickCategories) if(indexOfItemById(Data.quickCategories, id) == -1)
							Data.quickCategories.add(getItemById(Category.class, id));
					}
				}

				if(dataSnapshot.hasChild("groupType")) {
					String groupType = dataSnapshot.child("groupType").getValue(String.class);
					if(groupType != null && !groupType.isEmpty() && Data.groupType != ListFragment.GroupType.valueOf(groupType))
						Data.groupType = ListFragment.GroupType.valueOf(groupType);
				}
			}
			saveSettings(context);
			if(callback != null)
				callback.run();
		}));
	}

	private static void synchronizeSettingsExternally(DatabaseReference settings, Runnable callback) {
		settings.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			if(mergeConflicts || cleanUpDatabase) {
				Boolean vibrationEnabled = dataSnapshot.child("vibrationEnabled").getValue(Boolean.class);
				if(vibrationEnabled == null || Data.vibrationEnabled != vibrationEnabled)
					settings.child("vibrationEnabled").setValue(Data.vibrationEnabled);

				Boolean quickAddEnabled = dataSnapshot.child("quickAddEnabled").getValue(Boolean.class);
				if(quickAddEnabled == null || Data.quickAddEnabled != quickAddEnabled)
					settings.child("quickAddEnabled").setValue(Data.quickAddEnabled);

				ArrayList<String> strings = dataSnapshot.child("quickCategories").getValue(new GenericTypeIndicator<ArrayList<String>>(){});
				ArrayList<UUID> ids = Identifiable.getIds(Data.quickCategories);
				if(strings == null || !(ids.containsAll(Identifiable.getIdsFromStringIds(strings)) && Identifiable.getIdsFromStringIds(strings).containsAll(ids)))
					settings.child("quickCategories").setValue(Identifiable.getStringIdsFromIds(ids));

				String groupType = dataSnapshot.child("groupType").getValue(String.class);
				if(groupType == null || groupType.isEmpty() || Data.groupType != ListFragment.GroupType.valueOf(groupType))
					settings.child("groupType").setValue(Data.groupType.toString());
			}
			if(callback != null)
				callback.run();
		}));
	}


	private static void synchronizeColors(Context context, FirebaseUser currentUser, Runnable callback) {
		FirebaseDatabase database = FirebaseDatabase.getInstance();
		DatabaseReference colors = database.getReference(currentUser.getUid()).child(Constants.CUSTOM_COLORS);

		if(cleanUpDatabase)
			colors.removeValue((databaseError, databaseReference) -> synchronizeColorsExternally(context, colors, callback));
		else if(!mergeLocally || !mergeConflicts)
			synchronizeColorsExternally(context, colors, () -> synchronizeColorsLocally(context, colors, callback));
		else synchronizeColorsLocally(context, colors, () -> synchronizeColorsExternally(context, colors, callback));
	}

	private static void synchronizeColorsLocally(Context context, DatabaseReference colors, Runnable callback) {
		colors.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			for(DataSnapshot data : dataSnapshot.getChildren()) {
				Color color = Color.getColor(data);

				int index = indexOfItemById(Color.class, color.getId());
				if(index != -1 && !mergeConflicts && allColors.get(index).getColor() != color.getColor()) {
					color.setId(UUID.randomUUID());
					customColorsAmount++;
					allColors.add(color);
				} else if(index == -1) {
					customColorsAmount++;
					allColors.add(color);
				} else if(mergeConflicts)
					allColors.set(index, color);
			}
			saveColors(context);
			if(callback != null)
				callback.run();
		}));
	}

	private static void synchronizeColorsExternally(Context context, DatabaseReference colors, Runnable callback) {
		if(customColorsAmount == 0 && callback != null) callback.run();
		AtomicInteger done = new AtomicInteger();
		colors.addListenerForSingleValueEvent(new OnDataChangeListener(dataSnapshot -> {
			for(int i = allColors.size() - customColorsAmount; i < allColors.size(); i++) {
				Color color = allColors.get(i);
				if(!mergeConflicts && dataSnapshot.hasChild(color.getId().toString())) {
					Color c = Color.getColor(dataSnapshot.child(color.getId().toString()));
					if(c.getColor() != color.getColor())
						color.setId(UUID.randomUUID());
				}
				done.getAndIncrement();
				uploadColor(colors, color);
				if(done.get() == customColorsAmount && callback != null)
					callback.run();
			}
			saveCategories(context);
		}));
	}

	private static void uploadColor(DatabaseReference categories, Color color) {
		DatabaseReference colorRef = categories.child(color.getId().toString());
		colorRef.child("color").setValue(color.getColor());
		colorRef.child("id").setValue(color.getId().toString());
	}


	public static void saveData(Context context) {
		saveNotes(context);
		saveCategories(context);
		saveSettings(context);
		saveColors(context);
	}

	public static void saveNotes(Context context) {
		SharedPreferences.Editor sP = context.getSharedPreferences(Constants.NOTES, Context.MODE_MULTI_PROCESS).edit();
		sP.putInt("size", allNotes.size());
		for(int i = 0; i < allNotes.size(); i++) {
			Note note = allNotes.get(i);
			Alarm alarm = note.getAlarm();

			if(alarm != null) {
				sP.putBoolean("repeat_" + i, alarm.isRepeat());
				sP.putBoolean("selectedMode_" + i, alarm.isSelectedMode());
				sP.putBoolean("untilMode_" + i, alarm.isUntilMode());
				sP.putInt("everyAmount_" + i, alarm.getEveryAmount());
				sP.putInt("everyPeriod_" + i, alarm.getEveryPeriod());
				sP.putInt("repeatCount_" + i, alarm.getRepeatCount());
				sP.putString("date_" + i, Utils.dateFormat1.format(alarm.getDate().getTime()));
				sP.putString("untilDate_" + i, Utils.dateFormat4.format(alarm.getUntilDate().getTime()));
				for(int j = 0; j < 7; j++)
					sP.putBoolean("selectedDay_" + i + "_" + j, alarm.getSelectedDays()[j]);
			}

			sP.putString("content_" + i, note.getContent());
			sP.putBoolean("alarmSet_" + i, note.isAlarmSet());
			sP.putBoolean("silent_" + i, note.isSilent());
			sP.putString("categoryID_" + i, note.getCategoryID().toString());
			sP.putInt("color_" + i, note.getColor());
			sP.putInt("lockState_" + i, note.getLockState());
			sP.putBoolean("checked_" + i, note.isChecked());
			sP.putString("id_" + i, note.getId().toString());
		}
		sP.apply();
	}

	public static void saveCategories(Context context) {
		SharedPreferences.Editor sP = context.getSharedPreferences(Constants.CATEGORIES, Context.MODE_MULTI_PROCESS).edit();
		sP.putInt("size", allCategories.size());
		for(int i = 0; i < allCategories.size(); i++) {
			Category category = allCategories.get(i);
			sP.putString("name_" + i, category.getName());
			sP.putString("icon_" + i, category.getIcon());
			sP.putInt("color_" + i, category.getColor());
			sP.putString("id_" + i, category.getId().toString());
		}
		sP.apply();
	}

	public static void saveSettings(Context context) {
		SharedPreferences.Editor sP = context.getSharedPreferences(Constants.SETTINGS, Context.MODE_MULTI_PROCESS).edit();
		sP.putString("ringtoneTitle", selectedRingtone.getTitle());
		sP.putString("ringtoneUri", selectedRingtone.getUri().toString());
		sP.putBoolean("vibrationEnabled", vibrationEnabled);
		sP.putBoolean("mergeConflicts", mergeConflicts);
		sP.putBoolean("mergeLocally", mergeLocally);
		sP.putBoolean("cleanUpDatabase", cleanUpDatabase);
		sP.putBoolean("quickAddEnabled", quickAddEnabled);
		sP.putString("groupType", groupType.toString());

		sP.putInt("quickCategories", quickCategories.size());
		for(int i = 0; i < quickCategories.size(); i++)
			sP.putString("quickCategoryId_" + i, quickCategories.get(i).getId().toString());

		sP.apply();
	}

	public static void saveColors(Context context) {
		SharedPreferences.Editor sP = context.getSharedPreferences(Constants.CUSTOM_COLORS, Context.MODE_MULTI_PROCESS).edit();
		sP.putInt("size", customColorsAmount);

		for(int i = 0; i < customColorsAmount; i++) {
			sP.putInt("color_" + i, allColors.get(allColors.size() - customColorsAmount + i).getColor());
			sP.putString("id_" + i, allColors.get(allColors.size() - customColorsAmount + i).getId().toString());
		}

		sP.apply();
	}

	//////////////////// End saving data ////////////////////




	public static <T extends Identifiable> T getItemById(Class<T> clazz, UUID id) {
		if(clazz.isAssignableFrom(Note.class) && allNotes != null) for(Note n : allNotes) if(n.getId().equals(id)) return clazz.cast(n);
		if(clazz.isAssignableFrom(Category.class) && allCategories != null) for(Category c : allCategories) if(c.getId().equals(id)) return clazz.cast(c);
		if(clazz.isAssignableFrom(Color.class) && allColors != null) for(Color c : allColors) if(c.getId().equals(id)) return clazz.cast(c);
		if(clazz.isAssignableFrom(Icon.class) && allIcons != null) for(Icon i : allIcons) if(i.getId().equals(id)) return clazz.cast(i);
		return null;
	}

	public static <T extends Identifiable> int indexOfItemById(Class<T> clazz, UUID id) {
		if(clazz.isAssignableFrom(Note.class) && allNotes != null) return indexOfItemById(allNotes, id);
		if(clazz.isAssignableFrom(Category.class) && allCategories != null) return indexOfItemById(allCategories, id);
		if(clazz.isAssignableFrom(Color.class) && allColors != null) return indexOfItemById(allColors, id);
		if(clazz.isAssignableFrom(Icon.class) && allIcons != null) return indexOfItemById(allIcons, id);
		return -1;
	}

	public static <T extends Identifiable> int indexOfItemById(ArrayList<T> array, UUID id) {
		for(int i = 0; i < array.size(); i++) if(array.get(i).getId().equals(id)) return i;
		return -1;
	}
}
