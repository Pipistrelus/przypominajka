package com.infinitemind.przypominajka.models;

import com.intrusoft.sectionedrecyclerview.Section;

import java.util.ArrayList;
import java.util.List;

public class SectionHeader implements Section<Note> {

	private ArrayList<Note> childList;
	private Category category;
	private boolean show;

	public SectionHeader(ArrayList<Note> childList, Category category) {
		this.childList = childList;
		this.category = category;
		this.show = true;
	}

	@Override
	public List<Note> getChildItems() {
		return childList;
	}

	public void setChildList(ArrayList<Note> childList) {
		this.childList = childList;
	}

	public Category getCategory() {
		return category;
	}

	public static int countNotes(ArrayList<SectionHeader> sections) {
		return getNotes(sections).size();
	}

	public static ArrayList<Note> getNotes(ArrayList<SectionHeader> sections) {
		ArrayList<Note> notes = new ArrayList<>();
		for(int i = 0; i < sections.size(); i++)
			notes.addAll(sections.get(i).getChildItems());
		return notes;
	}

	public boolean isShow() {
		return show;
	}

	public void setShow(boolean show) {
		this.show = show;
	}
}
