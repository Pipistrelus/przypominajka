package com.infinitemind.przypominajka.models;

import android.net.Uri;

import java.util.ArrayList;
import java.util.UUID;

public class Ringtone extends Identifiable {
	private String title;
	private Uri uri;

	public Ringtone(String title, Uri uri) {
		this.title = title;
		this.uri = uri;
		setId(UUID.randomUUID());
	}


	public static int indexOf(ArrayList<Ringtone> ringtones, Uri uri) {
		for(int i = 0; i < ringtones.size(); i++)
			if(ringtones.get(i).getUri().equals(uri)) return i;
		return -1;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Uri getUri() {
		return uri;
	}

	public void setUri(Uri uri) {
		this.uri = uri;
	}
}
