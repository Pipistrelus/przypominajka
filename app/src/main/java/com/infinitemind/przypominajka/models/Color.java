package com.infinitemind.przypominajka.models;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.AppCompatTextView;

import com.google.firebase.database.DataSnapshot;
import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.interfaces.DataRunnable;
import com.infinitemind.przypominajka.utils.Utils;
import com.infinitemind.przypominajka.views.CustomFlagView;
import com.skydoves.colorpickerpreference.ColorPickerView;

import java.util.ArrayList;
import java.util.UUID;

public class Color extends Identifiable {
	private int color;

	public Color(int color) {
		this.color = color;
		this.id = UUID.randomUUID();
		setSelected(false);
	}


	public static ArrayList<Color> createArrayList(ArrayList<Integer> intColors) {
		ArrayList<Color> colors = new ArrayList<>();
		for(int i : intColors) colors.add(new Color(i));
		return colors;
	}

	public static int indexOf(ArrayList<Color> colors, int color) {
		for(int i = 0; i < colors.size(); i++)
			if(colors.get(i).getColor() == color)
				return i;
		return -1;
	}

	public static Color getColor(DataSnapshot data) {
		Color color = new Color(data.child("color").getValue(Integer.class));
		color.setId(UUID.fromString(data.child("id").getValue(String.class)));
		return color;
	}

	public static void showAddColorDialog(Activity activity, DataRunnable<Dialog> onDoneClickListener) {
		Utils.makeDialog(activity, R.layout.dialog_add_color, dialog -> {
			ColorPickerView colorPicker = dialog.findViewById(R.id.colorPicker);
			colorPicker.setFlagView(new CustomFlagView(activity));

			dialog.findViewById(R.id.cancelButton).setOnClickListener(view -> dialog.dismiss());

			dialog.findViewById(R.id.doneButton).setOnClickListener(view -> {
				if(onDoneClickListener != null)
					onDoneClickListener.run(dialog);
			});
		});
	}

	public static void showDeleteColorDialog(Activity activity, DataRunnable<Dialog> onYesClickListener) {
		Utils.makeDialog(activity, R.layout.dialog_confirmation, dialog -> {
			((AppCompatTextView) dialog.findViewById(R.id.text)).setText(activity.getResources().getString(R.string.delete_color_confirmation));

			dialog.findViewById(R.id.noButton).setOnClickListener(view -> dialog.dismiss());

			dialog.findViewById(R.id.yesButton).setOnClickListener(view -> {
				if(onYesClickListener != null)
					onYesClickListener.run(dialog);
			});
		});
	}


	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}
}
