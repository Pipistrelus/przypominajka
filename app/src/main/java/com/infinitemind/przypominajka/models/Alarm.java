package com.infinitemind.przypominajka.models;

import com.infinitemind.przypominajka.utils.Utils;

import java.util.Arrays;
import java.util.Calendar;

public class Alarm {
	private Calendar date;
	private Calendar untilDate;
	private boolean[] selectedDays;
	private boolean repeat;
	private boolean untilMode;
	private boolean selectedMode;
	private int repeatCount;
	private int everyAmount;
	private int everyPeriod;

	public Alarm(Calendar date, boolean repeat, boolean untilMode, Calendar untilDate, int repeatCount, boolean selectedMode, boolean[] selectedDays, int everyAmount, int everyPeriod) {
		this.date = date;
		this.repeat = repeat;
		this.untilMode = untilMode;
		this.untilDate = untilDate;
		this.repeatCount = repeatCount;
		this.selectedMode = selectedMode;
		this.selectedDays = selectedDays;
		this.everyAmount = everyAmount;
		this.everyPeriod = everyPeriod;
	}

	public Alarm() {
		this(Calendar.getInstance(), false, false, Calendar.getInstance(), 0, false, new boolean[7], 1, 0);
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public Calendar getUntilDate() {
		return untilDate;
	}

	public void setUntilDate(Calendar untilDate) {
		this.untilDate = untilDate;
	}

	public boolean[] getSelectedDays() {
		return selectedDays;
	}

	public void setSelectedDays(boolean[] selectedDays) {
		this.selectedDays = selectedDays;
	}

	public boolean isRepeat() {
		return repeat;
	}

	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
	}

	public boolean isUntilMode() {
		return untilMode;
	}

	public void setUntilMode(boolean untilMode) {
		this.untilMode = untilMode;
	}

	public boolean isSelectedMode() {
		return selectedMode;
	}

	public void setSelectedMode(boolean selectedMode) {
		this.selectedMode = selectedMode;
	}

	public int getRepeatCount() {
		return repeatCount;
	}

	public void setRepeatCount(int repeatCount) {
		this.repeatCount = repeatCount;
	}

	public int getEveryAmount() {
		return everyAmount;
	}

	public void setEveryAmount(int everyAmount) {
		this.everyAmount = everyAmount;
	}

	public int getEveryPeriod() {
		return everyPeriod;
	}

	public void setEveryPeriod(int everyPeriod) {
		this.everyPeriod = everyPeriod;
	}

	public boolean equals(Alarm alarm) {
		return alarm.getDate().equals(date) &&
				alarm.getUntilDate().equals(untilDate) &&
				Arrays.equals(alarm.getSelectedDays(), selectedDays) &&
				alarm.getEveryPeriod() == everyPeriod &&
				alarm.getEveryAmount() == everyAmount &&
				alarm.getRepeatCount() == repeatCount &&
				alarm.isUntilMode() == untilMode &&
				alarm.isSelectedMode() == selectedMode &&
				alarm.isRepeat() == repeat;
	}

	//Returns time for the next alarm
	private int getPeriod(int period) {
		switch(period) {
			case 0: return Calendar.MINUTE;
			case 1: return Calendar.HOUR;
			case 2: return Calendar.DAY_OF_MONTH;
			case 3: return Calendar.WEEK_OF_MONTH;
			case 4: return Calendar.MONTH;
		}
		return 0;
	}

	public long getNext() {
		Calendar next = Calendar.getInstance();
		if(!selectedMode)
			next.add(getPeriod(getEveryPeriod()), getEveryAmount());
		else {
			int dayOfWeek = next.get(Calendar.DAY_OF_WEEK) - 1; //0 - 6, Sunday - saturday
			dayOfWeek = (dayOfWeek == 0 ? 6 : dayOfWeek - 1); //0 - 6, Monday, Sunday
			for(int i = dayOfWeek + 1; i < dayOfWeek + 8; i++) {
				if(getSelectedDays()[i % 7]) {
					next.add(Calendar.DAY_OF_MONTH, i - dayOfWeek);
					break;
				}
			}
		}
		next.set(Calendar.SECOND, 0);
		next.set(Calendar.MILLISECOND, 0);
		return next.getTimeInMillis();
	}

	public boolean hasNext() {
		Calendar now = Calendar.getInstance();
		now.setTimeInMillis(getNext());
		return repeat && ((untilMode && untilDate.after(now)) || (!untilMode && repeatCount >= 0));
	}
}
