package com.infinitemind.przypominajka.models;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.GenericTypeIndicator;
import com.infinitemind.przypominajka.fragments.LockAddonFragment;
import com.infinitemind.przypominajka.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.UUID;

public class Note extends Identifiable {
	private String content;
	private Alarm alarm;
	private boolean checked;
	private boolean alarmSet;
	private boolean silent;
	private int color;
	@LockAddonFragment.LockState
	private int lockState;
	private UUID categoryID;

	public Note(String content, boolean silent, boolean alarmSet, Alarm alarm, int color, UUID categoryID) {
		this.content = content;
		this.silent = silent;
		this.alarmSet = alarmSet;
		this.alarm = alarm;
		this.color = color;
		this.categoryID = categoryID;
		this.lockState = LockAddonFragment.UNLOCKED;
		this.id = UUID.randomUUID();
	}

	public Note(Note note) {
		this(note.getContent(), note.isSilent(), note.isAlarmSet(), note.getAlarm(), note.getColor(), note.getCategoryID());
		this.lockState = note.getLockState();
		this.id = note.getId();
	}

	public Note() {
		this("", false, false, null, 0, UUID.randomUUID());
	}


	public static int countUnchecked(ArrayList<Note> notes) {
		int count = 0;
		for(Note note : notes)
			if(!note.isChecked()) count++;
		return count;
	}


	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public int getColor() {
		return color;
	}

	@LockAddonFragment.LockState
	public int getLockState() {
		return lockState;
	}

	public void setLockState(@LockAddonFragment.LockState int lockState) {
		this.lockState = lockState;
	}

	public UUID getCategoryID() {
		return categoryID;
	}

	public void setCategoryID(UUID categoryID) {
		this.categoryID = categoryID;
	}

	public boolean isAlarmSet() {
		return alarmSet;
	}

	public void setAlarmSet(boolean alarmSet) {
		this.alarmSet = alarmSet;
	}

	public boolean isSilent() {
		return silent;
	}

	public void setSilent(boolean silent) {
		this.silent = silent;
	}

	public Alarm getAlarm() {
		return alarm;
	}

	public void setAlarm(Alarm alarm) {
		this.alarm = alarm;
	}

	public boolean equals(Note note) {
		return note.getContent().equals(content) &&
				note.getCategoryID().equals(categoryID) &&
				note.getColor() == color &&
				note.getLockState() == lockState &&
				note.isChecked() == checked &&
				note.isAlarmSet() == alarmSet &&
				note.isSilent() == silent &&
				note.alarm.equals(alarm);
	}

	@Override
	public String toString() {
		return "Content: " + content + " categoryID: " + categoryID.toString() + " date: " + Utils.dateFormat1.format(alarm.getDate().getTime()) +
				" isRepeating: " + alarm.isRepeat() + " repeatCount: " + alarm.getRepeatCount() +
				" isUntilMode: " + alarm.isUntilMode() + " untilDate: " + Utils.dateFormat4.format(alarm.getUntilDate().getTime()) +
				" everyAmount: " + alarm.getEveryAmount() + " everyPeriod: " + alarm.getEveryPeriod() +
				" isSelectedMode: " + alarm.isSelectedMode() + " selectedDays: " + Arrays.toString(alarm.getSelectedDays()) + ".";
	}

	public static Note getNote(DataSnapshot data) {
		Note note = new Note();
		note.setContent(data.child("content").getValue(String.class));
		note.setCategoryID(UUID.fromString(data.child("categoryID").getValue(String.class)));
		note.setColor(data.child("color").getValue(Integer.class));
		note.setLockState(data.child("lockState").getValue(Integer.class));
		note.setSilent(data.child("silent").getValue(Boolean.class));
		note.setChecked(data.child("checked").getValue(Boolean.class));
		note.setAlarmSet(data.child("alarmSet").getValue(Boolean.class));
		note.setId(UUID.fromString(data.child("id").getValue(String.class)));

		note.setAlarm(new Alarm());
		ArrayList<Boolean> selectedDays = data.child("alarmSelectedDays").getValue(new GenericTypeIndicator<ArrayList<Boolean>>() {});
		for(int i = 0; i < selectedDays.size(); i++)
			note.getAlarm().getSelectedDays()[i] = selectedDays.get(i);
		try {
			Calendar date = Calendar.getInstance();
			date.setTimeInMillis(Utils.dateFormat1.parse(data.child("alarmDate").getValue(String.class)).getTime());
			note.getAlarm().setDate(date);

			Calendar untilDate = Calendar.getInstance();
			untilDate.setTimeInMillis(Utils.dateFormat4.parse(data.child("alarmUntilDate").getValue(String.class)).getTime());
			note.getAlarm().setUntilDate(untilDate);
		} catch(ParseException e) {
			e.printStackTrace();
		}
		note.getAlarm().setRepeat(data.child("alarmRepeat").getValue(Boolean.class));
		note.getAlarm().setUntilMode(data.child("alarmUntilMode").getValue(Boolean.class));
		note.getAlarm().setSelectedMode(data.child("alarmSelectedMode").getValue(Boolean.class));
		note.getAlarm().setRepeatCount(data.child("alarmRepeatCount").getValue(Integer.class));
		note.getAlarm().setEveryAmount(data.child("alarmEveryAmount").getValue(Integer.class));
		note.getAlarm().setEveryPeriod(data.child("alarmEveryPeriod").getValue(Integer.class));
		return note;
	}
}
