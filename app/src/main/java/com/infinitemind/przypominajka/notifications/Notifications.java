package com.infinitemind.przypominajka.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.opengl.Visibility;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.widget.RemoteViews;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.activities.AddNoteActivity;
import com.infinitemind.przypominajka.activities.SplashActivity;
import com.infinitemind.przypominajka.fragments.LockAddonFragment;
import com.infinitemind.przypominajka.models.Category;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.services.DeleteService;
import com.infinitemind.przypominajka.services.DoneService;
import com.infinitemind.przypominajka.services.UnlockService;
import com.infinitemind.przypominajka.utils.Constants;
import com.infinitemind.przypominajka.utils.Utils;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Notifications {

	public static void showNotification(Context context, Note note, boolean showSilently) {
		Data.loadData(context);
		Category category = Data.getItemById(Category.class, note.getCategoryID());
		String categoryName = category != null ? category.getName() : context.getResources().getString(R.string.reminder);
		String largeIcon = category != null ? category.getIcon() : context.getResources().getString(R.string.ic_dashboard);

		NotificationCompat.Builder notificationBuilder = getNotification(context, context.getResources().getString(R.string.reminders),
				note.getContent(), categoryName, largeIcon, R.drawable.ic_alarm, note.getColor(), note.getLockState());

		PendingIntent contentIntent = PendingIntent.getActivity(context, note.getId().hashCode() + 1,
				new Intent(context, AddNoteActivity.class).putExtra("id", note.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT);

		PendingIntent deleteIntent = PendingIntent.getService(context, note.getId().hashCode() + 2,
				new Intent(context, DeleteService.class).putExtra("id", note.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT);

		PendingIntent snoozeIntent = PendingIntent.getActivity(context, note.getId().hashCode() + 3,
				new Intent(context, AddNoteActivity.class).putExtra("id", note.getId().toString()).putExtra("getDate", true), PendingIntent.FLAG_UPDATE_CURRENT);

		PendingIntent doneIntent = PendingIntent.getService(context, note.getId().hashCode() + 4,
				new Intent(context, DoneService.class).putExtra("id", note.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT);

		PendingIntent unlockIntent = PendingIntent.getService(context, note.getId().hashCode() + 5,
				new Intent(context, UnlockService.class).putExtra("id", note.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT);

		notificationBuilder.addAction(R.drawable.ic_snooze, context.getResources().getString(R.string.snooze), snoozeIntent);

		if(note.getLockState() == LockAddonFragment.LOCKED) {
			notificationBuilder.setDeleteIntent(deleteIntent);
			notificationBuilder.addAction(R.drawable.ic_lock_open, context.getResources().getString(R.string.unlock), unlockIntent);
		} else notificationBuilder.addAction(R.drawable.ic_done, context.getResources().getString(R.string.done), doneIntent);

		notificationBuilder.setContentIntent(contentIntent);

		if(!showSilently && !note.isSilent()) {
			int flags = NotificationCompat.DEFAULT_LIGHTS;
			if(Data.vibrationEnabled) {
				flags |= NotificationCompat.DEFAULT_VIBRATE;
				notificationBuilder.setDefaults(flags);
			} if(Data.selectedRingtone == null && !Data.selectedRingtone.getTitle().equals(context.getResources().getString(R.string.silent))) {
				flags |= NotificationCompat.DEFAULT_SOUND;
				notificationBuilder.setDefaults(flags);
			} else notificationBuilder.setSound(Data.selectedRingtone.getUri());
		}

		Notification notification = notificationBuilder.build();
		if(note.getLockState() == LockAddonFragment.LOCKED)
			notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
		if(notificationManager != null) notificationManager.notify(note.getId().hashCode(), notification);
	}

	public static void hideNotification(Context context, int code) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
		if(notificationManager != null) notificationManager.cancel(code);
	}


	public static void showQuickAddNotification(Context context) {
		NotificationCompat.Builder notificationBuilder = getNotification(context, context.getResources().getString(R.string.quick_add_notification),
				context.getResources().getString(R.string.reminder), context.getResources().getString(R.string.add_a_note), null, R.drawable.ic_add,
				context.getResources().getColor(R.color.colorAccent1), LockAddonFragment.LOCKED);

		PendingIntent openIntent = PendingIntent.getActivity(context, Constants.OPEN_APP_REQUEST_CODE, new Intent(context, SplashActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
		PendingIntent contentIntent = PendingIntent.getActivity(context, Constants.ADD_NOTE_REQUEST_CODE, new Intent(context, AddNoteActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);

		notificationBuilder.setContentIntent(contentIntent);
		notificationBuilder.setPriority(NotificationCompat.PRIORITY_MAX);

		RemoteViews small = new RemoteViews(context.getPackageName(), R.layout.quick_add_notification_view_small);
		RemoteViews large = new RemoteViews(context.getPackageName(), R.layout.quick_add_notification_view_large);

		small.setOnClickPendingIntent(R.id.openAppButton, openIntent);
		large.setOnClickPendingIntent(R.id.openAppButton, openIntent);

<<<<<<< Updated upstream
		if(Data.quickCategories != null) {
			if(!Data.quickCategories.isEmpty()) {
				Category category = Data.quickCategories.get(0);
				if(category != null) {
					large.setViewVisibility(R.id.button1, View.VISIBLE);
					large.setImageViewBitmap(R.id.buttonIcon1, Utils.fontIconBitmap(context, category.getIcon(), category.getColor(), 32, false));
					large.setTextViewText(R.id.buttonText1, category.getName());
					large.setOnClickPendingIntent(R.id.button1, PendingIntent.getActivity(context, category.getId().hashCode(),
							new Intent(context, AddNoteActivity.class).putExtra("categoryID", category.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT));
				} else large.setViewVisibility(R.id.button2, View.GONE);
			} else large.setViewVisibility(R.id.button1, View.GONE);
			if(Data.quickCategories.size() > 1) {
				Category category = Data.quickCategories.get(1);
				if(category != null) {
					large.setViewVisibility(R.id.button2, View.VISIBLE);
					large.setImageViewBitmap(R.id.buttonIcon2, Utils.fontIconBitmap(context, category.getIcon(), category.getColor(), 32, false));
					large.setTextViewText(R.id.buttonText2, category.getName());
					large.setOnClickPendingIntent(R.id.button2, PendingIntent.getActivity(context, category.getId().hashCode(),
							new Intent(context, AddNoteActivity.class).putExtra("categoryID", category.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT));
				} else large.setViewVisibility(R.id.button2, View.GONE);
			} else large.setViewVisibility(R.id.button2, View.GONE);

			if(Data.quickCategories.size() > 2) {
				Category category = Data.quickCategories.get(2);
				if(category != null) {
					large.setViewVisibility(R.id.button3, View.VISIBLE);
					large.setImageViewBitmap(R.id.buttonIcon3, Utils.fontIconBitmap(context, category.getIcon(), category.getColor(), 32, false));
					large.setTextViewText(R.id.buttonText3, category.getName());
					large.setOnClickPendingIntent(R.id.button3, PendingIntent.getActivity(context, category.getId().hashCode(),
							new Intent(context, AddNoteActivity.class).putExtra("categoryID", category.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT));
				} else large.setViewVisibility(R.id.button2, View.GONE);
			} else large.setViewVisibility(R.id.button3, View.GONE);
=======
		Data.loadCategories(context);
		if(Data.allCategories.size() > 0) {
			Category category = Data.allCategories.get(0);
			large.setImageViewBitmap(R.id.buttonIcon1, Utils.fontIconBitmap(context, category.getIcon(), category.getColor(), 32, false));
			large.setTextViewText(R.id.buttonText1, category.getName());
			large.setOnClickPendingIntent(R.id.button1, PendingIntent.getActivity(context, category.getId().hashCode(),
					new Intent(context, AddNoteActivity.class).putExtra("categoryID", category.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT));

			if(Data.allCategories.size() > 1) {
				category = Data.allCategories.get(1);
				large.setImageViewBitmap(R.id.buttonIcon2, Utils.fontIconBitmap(context, category.getIcon(), category.getColor(), 32, false));
				large.setTextViewText(R.id.buttonText2, category.getName());
				large.setOnClickPendingIntent(R.id.button2, PendingIntent.getActivity(context, category.getId().hashCode(),
						new Intent(context, AddNoteActivity.class).putExtra("categoryID", category.getId().toString()), PendingIntent.FLAG_UPDATE_CURRENT));
			}
>>>>>>> Stashed changes
		}

		notificationBuilder.setCustomContentView(small);
		notificationBuilder.setCustomBigContentView(large);

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
		if(notificationManager != null) notificationManager.notify(Constants.OPEN_APP_NOTIFICATION_ID, notificationBuilder.build());
	}

	public static void hideQuickAddNotification(Context context) {
		hideNotification(context, Constants.OPEN_APP_NOTIFICATION_ID);
	}


	private static NotificationCompat.Builder getNotification(Context context, String channel, String content, String category, String largeIcon,
	                                                          int smallIcon, int color, @LockAddonFragment.LockState int lockState) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channel)
				.setSmallIcon(smallIcon)
				.setTicker(category)
				.setContentTitle(category)
				.setContentText(content)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(content))
				.setColor(color)
				.setOngoing(lockState == LockAddonFragment.LOCKED)
				.setShowWhen(true)
				.setAutoCancel(false)
				.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
				.setPriority(NotificationCompat.PRIORITY_MAX);
		if(largeIcon != null && !largeIcon.isEmpty()) builder.setLargeIcon(Utils.fontIconBitmap(context, largeIcon, color, 128, true));
		return builder;
	}
}
