package com.infinitemind.przypominajka.settings;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.res.ResourcesCompat;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.interfaces.DataRunnable;
import com.infinitemind.przypominajka.utils.Utils;

public class SettingsItem<T> extends FrameLayout {

	private Switch check;
	private TextView textSubtitle;
	private TextView textTitle;
	private ImageView image;
	private String title;
	private String subtitle;
	protected DataRunnable<T> onClickListener;

	public SettingsItem(Context context) {
		super(context);
		int dp5 = Utils.dpToPx(context, 5);
		int titleId = View.generateViewId();
		int subtitleId = View.generateViewId();

		setBackgroundColor(getResources().getColor(R.color.white));
		setClickable(true);
		TypedArray ta = context.obtainStyledAttributes(new int[] { android.R.attr.selectableItemBackground});
		setForeground(ta.getDrawable(0));
		ta.recycle();

		RelativeLayout rootView = new RelativeLayout(context);
		rootView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

		textTitle = new TextView(context);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(dp5 * 3, dp5, dp5 * 3, 0);
		textTitle.setLayoutParams(params);
		textTitle.setTypeface(ResourcesCompat.getFont(context, R.font.arcon));
		textTitle.setText(getTitle());
		textTitle.setTextColor(getResources().getColor(R.color.colorText3));
		textTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		textTitle.setId(titleId);

		textSubtitle = new TextView(context);
		params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.setMargins(dp5 * 3, 0, dp5 * 3, dp5);
		params.addRule(RelativeLayout.BELOW, titleId);
		textSubtitle.setLayoutParams(params);
		textSubtitle.setTypeface(ResourcesCompat.getFont(context, R.font.arcon));
		textSubtitle.setText(getSubtitle());
		textSubtitle.setTextColor(getResources().getColor(R.color.colorAccent1));
		textSubtitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
		textSubtitle.setId(subtitleId);

		check = new Switch(context);
		params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.CENTER_VERTICAL);
		params.setMargins(0, 0, dp5 * 3, 0);
		check.setLayoutParams(params);

		image = new ImageView(context);
		params = new RelativeLayout.LayoutParams(dp5 * 6, dp5 * 6);
		params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		params.addRule(RelativeLayout.CENTER_VERTICAL);
		params.setMargins(0, 0, dp5 * 3, 0);
		image.setColorFilter(getResources().getColor(R.color.colorAccent1));
		image.setImageResource(R.drawable.ic_arrow_right);
		image.setScaleType(ImageView.ScaleType.CENTER_CROP);
		image.setPadding(dp5, dp5, dp5, dp5);
		image.setLayoutParams(params);

		View divider = new View(context);
		params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, dp5 / 5);
		params.setMargins(dp5, 0, dp5, 0);
		params.addRule(RelativeLayout.BELOW, subtitleId);
		divider.setLayoutParams(params);
		divider.setBackgroundColor(Utils.getDarkerColor(getResources().getColor(R.color.white)));

		rootView.addView(textTitle);
		rootView.addView(textSubtitle);
		if(getType() == Type.checkable) rootView.addView(check);
		if(getType() == Type.withImage || getType() == Type.simple) rootView.addView(image);
		rootView.addView(divider);

		addView(rootView);
	}

	public void refreshTitle() {
		textTitle.setText(getTitle());
	}

	public void refreshSubtitle() {
		textSubtitle.setText(getSubtitle());
	}

	public SettingsItem refresh() {
		refreshTitle();
		refreshSubtitle();
		return this;
	}

	public SettingsItem setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public SettingsItem setSubtitle(String subtitle) {
		this.subtitle = subtitle;
		return this;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public ImageView getImage() {
		return image;
	}

	public Switch getSwitch() {
		return check;
	}

	public TextView getTextTitle() {
		return textTitle;
	}

	public TextView getTextSubtitle() {
		return textSubtitle;
	}

	public SettingsItem setOnClickListener(DataRunnable<T> onClickListener) {
		this.onClickListener = onClickListener;
		return this;
	}

	public SettingsItem<T> toggle(boolean enabled) {
		super.setEnabled(enabled);
		setAlpha(enabled ? 1 : 0.3f);
		setClickable(enabled);
		return this;
	}

	public Type getType() {
		return Type.simple;
	}

	public enum Type {
		simple, checkable, withImage
	}
}
