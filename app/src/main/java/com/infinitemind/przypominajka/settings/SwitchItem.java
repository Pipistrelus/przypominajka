package com.infinitemind.przypominajka.settings;

import android.content.Context;

public class SwitchItem extends SettingsItem<Boolean> {

	private boolean checked;

	public SwitchItem(Context context) {
		super(context);

		setOnClickListener((OnClickListener) view -> getSwitch().setChecked(!getSwitch().isChecked()));
		getSwitch().setOnCheckedChangeListener((compoundButton, b) -> {
			if(onClickListener != null) onClickListener.run(b);
		});
	}

	public SwitchItem setChecked(boolean checked) {
		getSwitch().setChecked(this.checked = checked);
		return this;
	}

	public boolean isChecked() {
		return checked;
	}

	@Override
	public Type getType() {
		return Type.checkable;
	}
}
