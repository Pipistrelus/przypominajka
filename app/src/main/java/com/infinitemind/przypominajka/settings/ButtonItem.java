package com.infinitemind.przypominajka.settings;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.utils.Utils;

public class ButtonItem extends CardView {

	private Runnable onClickListener;
	private TextView textContent;
	private String content;
	private int color;

	public ButtonItem(Context context) {
		super(context);

		int dp3 = Utils.dpToPx(context, 3);
		setRadius(dp3 / 3);
		setCardElevation(dp3);
		setClickable(true);
		TypedValue outValue = new TypedValue();
		context.getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
		setForeground(context.getDrawable(outValue.resourceId));

		LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		params1.setMargins(dp3 * 5, dp3 * 5, dp3 * 5, dp3 * 5);
		params1.gravity = Gravity.CENTER_HORIZONTAL;
		setLayoutParams(params1);

		textContent = new TextView(context);
		CardView.LayoutParams params = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		params.setMargins(dp3 * 5, dp3 / 3 * 7, dp3 * 5, dp3 / 3 * 7);
		textContent.setLayoutParams(params);
		textContent.setText(getContent());
		textContent.setTextColor(getResources().getColor(R.color.colorText1));
		textContent.setTextAlignment(TEXT_ALIGNMENT_GRAVITY);
		textContent.setGravity(Gravity.CENTER);
		textContent.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		textContent.setTypeface(ResourcesCompat.getFont(context, R.font.arcon));

		addView(textContent);

		setOnClickListener(view -> onClickListener.run());
	}

	public ButtonItem setColor(int color) {
		this.color = color;
		return this;
	}

	public int getColor() {
		return color;
	}

	public void refreshColor() {
		setCardBackgroundColor(getColor());
	}

	public ButtonItem setContent(String content) {
		this.content = content;
		return this;
	}

	public String getContent() {
		return content;
	}

	public void refreshContent() {
		textContent.setText(getContent());
	}

	public ButtonItem refresh() {
		refreshColor();
		refreshContent();
		return this;
	}

	public ButtonItem toggle(boolean enabled) {
		super.setEnabled(enabled);
		setAlpha(enabled ? 1 : 0.3f);
		setClickable(enabled);
		return this;
	}

	public ButtonItem setOnClickListener(Runnable onClickListener) {
		this.onClickListener = onClickListener;
		return this;
	}
}
