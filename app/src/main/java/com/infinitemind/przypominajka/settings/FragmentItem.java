package com.infinitemind.przypominajka.settings;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;

import com.infinitemind.przypominajka.views.ErrorMessageView;

public class FragmentItem extends SettingsItem<Void> {

	private OnFragmentChangeListener onFragmentChangeListener;
	private Fragment fragment;

	public FragmentItem(Context context) {
		super(context);

		setOnClickListener((OnClickListener) view -> onFragmentChangeListener.onFragmentChange(fragment));
	}

	public Fragment getFragment() {
		return fragment;
	}

	public FragmentItem setFragment(Fragment fragment) {
		this.fragment = fragment;
		return this;
	}

	public FragmentItem setOnFragmentChangeListener(OnFragmentChangeListener onFragmentChangeListener) {
		this.onFragmentChangeListener = onFragmentChangeListener;
		return this;
	}

	public interface OnFragmentChangeListener {
		void onFragmentChange(Fragment fragment);
	}
}
