package com.infinitemind.przypominajka.settings;

import android.content.Context;

public class SimpleItem extends SettingsItem<Void> {

	public SimpleItem(Context context) {
		super(context);

		setOnClickListener((OnClickListener) view -> onClickListener.run(null));
	}

	@Override
	public Type getType() {
		return Type.simple;
	}
}
