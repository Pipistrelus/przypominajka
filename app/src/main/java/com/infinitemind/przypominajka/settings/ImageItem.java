package com.infinitemind.przypominajka.settings;

import android.content.Context;
import android.support.annotation.DrawableRes;

import com.infinitemind.przypominajka.R;

public class ImageItem extends SettingsItem<Void> {

	public ImageItem(Context context) {
		super(context);

		setOnClickListener((OnClickListener) view -> onClickListener.run(null));
	}

	public ImageItem setImageResource(@DrawableRes int id) {
		getImage().setImageResource(id);
		getImage().setColorFilter(getResources().getColor(R.color.transparent));
		return this;
	}

	@Override
	public Type getType() {
		return Type.withImage;
	}
}
