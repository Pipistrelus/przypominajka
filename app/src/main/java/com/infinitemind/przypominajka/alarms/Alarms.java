package com.infinitemind.przypominajka.alarms;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.infinitemind.przypominajka.models.Note;

public class Alarms {

	public static void setAlarm(Context context, Note note, boolean first) {
		if(note.isAlarmSet() && note.getAlarm() != null) {
			Intent intent = new Intent(context, AlarmReceiver.class);
			intent.putExtra("id", note.getId().toString());
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, note.getId().hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
			AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
			if(alarmManager != null)
				alarmManager.setExact(AlarmManager.RTC_WAKEUP, first ? note.getAlarm().getDate().getTimeInMillis() : note.getAlarm().getNext(), pendingIntent);
		}
	}

	public static void cancelAlarm(Context context, Note note) {
		Intent intent = new Intent(context, AlarmReceiver.class);
		intent.putExtra("id", note.getId().toString());
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, note.getId().hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if(alarmManager != null) alarmManager.cancel(pendingIntent);
	}
}
