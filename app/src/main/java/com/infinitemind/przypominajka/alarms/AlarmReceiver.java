package com.infinitemind.przypominajka.alarms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.notifications.Notifications;

import java.util.UUID;

public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Data.loadNotes(context);
		String stringID = intent.getStringExtra("id");
		if(stringID != null && !stringID.isEmpty()) {
			Note note = Data.getItemById(Note.class, UUID.fromString(stringID));
			if(note != null) {
				Notifications.showNotification(context, note, false);

				if(!note.getAlarm().isUntilMode()) {
					int repeatCount = note.getAlarm().getRepeatCount();
					if(repeatCount > 0) note.getAlarm().setRepeatCount(repeatCount - 1);
					if(repeatCount == 1) note.getAlarm().setRepeat(false);
					Data.saveNotes(context);
				}

				if(note.getAlarm().hasNext()) {
					Alarms.cancelAlarm(context, note);
					Alarms.setAlarm(context, note, false);
				}
			}
		}
	}
}
