package com.infinitemind.przypominajka.views;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.utils.Utils;

public class CustomDatePicker extends DatePicker {
	public CustomDatePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		Utils.setFont(context, this);
	}
}
