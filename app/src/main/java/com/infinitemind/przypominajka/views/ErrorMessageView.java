package com.infinitemind.przypominajka.views;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.interfaces.DataRunnable;
import com.infinitemind.przypominajka.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

public class ErrorMessageView extends LinearLayout {

	private TextView message;
	private ImageView icon;
	private Type type = Type.OFFLINE;

	public ErrorMessageView(Context context) {
		super(context);
	}

	public ErrorMessageView(Activity activity) {
		this(activity.getApplicationContext());
		addIcon();
		addMessage();

		setGravity(Gravity.CENTER);

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				activity.runOnUiThread(() -> {
					ViewGroup parent = (ViewGroup) getParent();
					parent.removeView(ErrorMessageView.this);
					TransitionManager.beginDelayedTransition(parent, new AutoTransition());
				});
			}
		}, 1000);

		switch(type) {
			case OFFLINE:
				setBackgroundColor(getResources().getColor(R.color.colorAccent3));
				message.setText(getResources().getString(R.string.error_offline));
				icon.setImageResource(R.drawable.ic_error);
				break;
		}
	}

	public ErrorMessageView(Activity activity, Type type) {
		super(activity);
		this.type = type;
	}

	private void addIcon() {
		icon = new ImageView(getContext());
		icon.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		icon.setImageResource(R.drawable.ic_error);
		icon.setColorFilter(getResources().getColor(R.color.colorText1));

		addView(icon);
	}

	private void addMessage() {
		int dp10 = Utils.dpToPx(getContext(), 10);
		message = new TextView(getContext());
		message.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		message.setGravity(Gravity.CENTER);
		message.setTextAlignment(TEXT_ALIGNMENT_GRAVITY);
		message.setTypeface(ResourcesCompat.getFont(getContext(), R.font.arcon));
		message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		message.setTextColor(getResources().getColor(R.color.colorText1));
		message.setPadding(dp10, dp10, dp10, dp10);
		message.setText(getResources().getString(R.string.error_offline));

		addView(message);
	}

	public enum Type {
		OFFLINE
	}
}
