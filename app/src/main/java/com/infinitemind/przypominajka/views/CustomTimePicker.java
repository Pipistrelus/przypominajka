package com.infinitemind.przypominajka.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TimePicker;

import com.infinitemind.przypominajka.utils.Utils;

public class CustomTimePicker extends TimePicker {
	public CustomTimePicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		Utils.setFont(context, this);
	}
}
