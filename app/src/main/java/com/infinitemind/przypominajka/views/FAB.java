package com.infinitemind.przypominajka.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.utils.Utils;

public class FAB extends CardView {

	private final int dp5 = Utils.dpToPx(getContext(), 5);
	private float elevation;
	private int padding;
	private int color;
	private int icon;

	public FAB(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);

		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FAB, 0, 0);

		try {
			elevation = a.getDimension(R.styleable.FAB_fab_elevation, dp5);
			padding = (int) a.getDimension(R.styleable.FAB_fab_padding, dp5 * 2);
			icon = a.getResourceId(R.styleable.FAB_fab_icon, R.drawable.ic_add);
			color = a.getColor(R.styleable.FAB_fab_color, getResources().getColor(R.color.colorAccent1));
		} catch(Exception ignored) {}

		a.recycle();

		constructFAB();
	}

	private void constructFAB() {
		setCardBackgroundColor(color);
		setCardElevation(elevation);
		setClickable(true);

		RelativeLayout rootView = new RelativeLayout(getContext());
		rootView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

		ImageView button = new ImageView(getContext());
		button.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
		button.setImageResource(icon);
		button.setPadding(padding, padding, padding, padding);
		button.setColorFilter(getResources().getColor(R.color.white));

		rootView.addView(button);
		addView(rootView);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		setRadius(Math.min(getWidth(), getHeight()) / 2);
	}
}
