package com.infinitemind.przypominajka.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;

import com.infinitemind.przypominajka.R;
import com.skydoves.colorpickerpreference.ColorEnvelope;
import com.skydoves.colorpickerpreference.FlagView;

@SuppressLint("ViewConstructor")
public class CustomFlagView extends FlagView {

	public CustomFlagView(Context context) {
		super(context, R.layout.custom_flag_view);
	}

	@Override
	public void onRefresh(ColorEnvelope colorEnvelope) {
		((AppCompatTextView) findViewById(R.id.colorPreview)).setTextColor(colorEnvelope.getColor());
		((AppCompatTextView) findViewById(R.id.colorText)).setText(String.format("#%s", colorEnvelope.getColorHtml()));
		findViewById(R.id.colorPreview).setRotation(getRotation());
		findViewById(R.id.colorText).setRotation(getRotation());
	}
}