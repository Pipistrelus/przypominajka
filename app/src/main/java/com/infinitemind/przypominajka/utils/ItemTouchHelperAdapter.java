package com.infinitemind.przypominajka.utils;

public interface ItemTouchHelperAdapter {
	default void onItemMove(int fromPosition, int toPosition) {}
	default void onItemSwipe(int fromPosition, int toPosition) {}
}