package com.infinitemind.przypominajka.utils;

public class Constants {
	public static final int OPEN_APP_REQUEST_CODE = 0;
	public static final int OPEN_APP_NOTIFICATION_ID = 1;
	public static final int ADD_NOTE_REQUEST_CODE = 2;
	public static final int SIGN_IN_REQUEST_CODE = 3;
	public static final int LOCATION_REQUEST_CODE = 4;
	public static final String NOTES = "Notes";
	public static final String CATEGORIES = "Categories";
	public static final String SETTINGS = "Settings";
	public static final String CUSTOM_COLORS = "Custom Colors";
	public static int DURATION = 300;
}
