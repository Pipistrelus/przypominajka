package com.infinitemind.przypominajka.utils;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.TextView;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.interfaces.DataRunnable;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class Utils {

	//////////////////// Math stuff ////////////////////

	public static float map(float value, float istart, float istop, float ostart, float ostop) {
		return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
	}

	//////////////////// End math stuff ////////////////////

	//////////////////// Date stuff ////////////////////

	public static SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
	public static SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());
	public static SimpleDateFormat dateFormat3 = new SimpleDateFormat("HH:mm", Locale.getDefault());
	public static SimpleDateFormat dateFormat4 = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
	public static SimpleDateFormat dateFormat5 = new SimpleDateFormat("EEE", Locale.getDefault());

	//////////////////// End date stuff ////////////////////


	//////////////////// Screen stuff ////////////////////

	public static int dpToPx(Context context, int dp) {
		return (int) (context.getResources().getDisplayMetrics().density * dp);
	}

	public static Point getScreenSize(Context context) {
		return new Point(context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels);
	}

	public static void hideKeyboard(Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm != null) {
			View view = activity.getCurrentFocus();
			if(view == null) view = new View(activity);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	public static void showKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(imm != null)
			imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
	}

	public static void makeDialog(Activity activity, @LayoutRes int layout, DataRunnable<Dialog> callback) {
		Dialog dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		Window window = dialog.getWindow();
		if(window != null) {
			window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
			WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
			layoutParams.dimAmount = 0.5f;
			dialog.getWindow().setAttributes(layoutParams);
		}
		dialog.setContentView(layout);
		callback.run(dialog);
		if(!dialog.isShowing())
			dialog.show();
	}

	public static void setFont(Context context, ViewGroup group) {
		for(int i = 0; i < group.getChildCount(); i++) {
			if(group.getChildAt(i) instanceof ViewGroup)
				setFont(context, (ViewGroup) group.getChildAt(i));
			if(group.getChildAt(i) instanceof TextView)
				((TextView) group.getChildAt(i)).setTypeface(ResourcesCompat.getFont(context, R.font.arcon));
		}
	}

	public static void disableChildrenClicks(ViewGroup group) {
		for(int i = 0; i < group.getChildCount(); i++) {
			View child = group.getChildAt(i);
			if(child instanceof ViewGroup)
				disableChildrenClicks((ViewGroup) child);
			if(child.isClickable()) {
				child.setTag("clickable");
				child.setClickable(false);
			}
		}
	}

	public static void enableChildrenClicks(ViewGroup group) {
		for(int i = 0; i < group.getChildCount(); i++) {
			if(group.getChildAt(i) instanceof ViewGroup)
				enableChildrenClicks((ViewGroup) group.getChildAt(i));
			if(group.getChildAt(i).getTag() == "clickable")
				group.getChildAt(i).setClickable(true);
		}
	}

	//////////////////// End screen stuff ////////////////////


	//////////////////// Color stuff ////////////////////

	public static int getDarkerColor(@ColorInt int color) {
		float[] hsv = new float[3];
		Color.colorToHSV(color, hsv);
		hsv[2] *= 0.9f;
		return Color.HSVToColor(hsv);
	}

	public static boolean isColorDark(@ColorInt int color) {
		double darkness = 1 - (0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color)) / 255;
		return darkness >= 0.5;
	}

	public static int adjustAlpha(@ColorInt int color, float factor) {
		return Color.argb(Math.round(Color.alpha(color) * factor), Color.red(color), Color.green(color), Color.blue(color));
	}

	public static void tint(@NonNull CheckBox checkBox, @ColorInt int color) {
		final int disabledColor = getDisabledColor(checkBox.getContext());
		checkBox.setButtonTintList(new ColorStateList(new int[][]{
				new int[]{android.R.attr.state_enabled, -android.R.attr.state_checked},
				new int[]{android.R.attr.state_enabled, android.R.attr.state_checked},
				new int[]{-android.R.attr.state_enabled, -android.R.attr.state_checked},
				new int[]{-android.R.attr.state_enabled, android.R.attr.state_checked}
		}, new int[]{
				color, color,
				disabledColor, disabledColor
		}));
	}

	@ColorInt
	private static int getDisabledColor(Context context) {
		final int primaryColor = getThemeAttrColor(context, android.R.attr.textColorPrimary);
		final int disabledColor = isColorDark(primaryColor) ? Color.WHITE : Color.BLACK;
		return adjustAlpha(disabledColor, 0.3f);
	}

	private static int getThemeAttrColor(@NonNull Context context, @AttrRes int attributeColor) {
		int[] attrs = new int[]{attributeColor};
		TypedArray ta = context.obtainStyledAttributes(attrs);
		int color = ta.getColor(0, Color.TRANSPARENT);
		ta.recycle();
		return color;
	}

	public static Bitmap fontIconBitmap(Context context, String text, int textColor, int size, boolean withCircle) {
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setTextSize(size);
		paint.setTypeface(ResourcesCompat.getFont(context, R.font.fontawesome));
		paint.setColor(textColor);
		paint.setTextAlign(Paint.Align.CENTER);
		Bitmap image = Bitmap.createBitmap(size * 2, size * 2, Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(image);
		if(withCircle) canvas.drawCircle(size, size, size, paint);
		paint.setColor(withCircle ? android.graphics.Color.WHITE : textColor);
		int xPos = (canvas.getWidth() / 2);
		int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));
		canvas.drawText(text, xPos, yPos, paint);
		return image;
	}

	//////////////////// End color stuff ////////////////////


	//////////////////// Internet stuff ////////////////////

	public static boolean isOffline(Context context) {
		//It's lagging with slow internet connection
		/*try {
			return (Runtime.getRuntime().exec("/system/bin/ping -c 1 8.8.8.8").waitFor() == 0);
		} catch(IOException | InterruptedException ignored) { }
		return false;*/

		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(connectivity == null) return true;
		else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if(info != null) for(NetworkInfo anInfo : info)
				if(anInfo.getState() == NetworkInfo.State.CONNECTED) return false;
		}
		return true;
	}

	public static boolean checkLocationServicesPermission(Context context) {
		boolean isAvailable = false;
		try {
			isAvailable = (Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE) != Settings.Secure.LOCATION_MODE_OFF);
		} catch (Settings.SettingNotFoundException e) {
			e.printStackTrace();
		}

		boolean coarsePermissionCheck = (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
		boolean finePermissionCheck = (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED);

		return isAvailable && (coarsePermissionCheck || finePermissionCheck);
	}

	//////////////////// End internet stuff ////////////////////
}
