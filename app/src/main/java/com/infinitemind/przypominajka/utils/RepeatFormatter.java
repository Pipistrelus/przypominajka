package com.infinitemind.przypominajka.utils;

import android.content.Context;
import android.support.annotation.Nullable;

import com.infinitemind.przypominajka.R;
import com.infinitemind.przypominajka.models.Alarm;

public class RepeatFormatter {
	public static String format(@Nullable Context context, Alarm alarm) {
		if(context == null || !alarm.isRepeat())
			return "";
		StringBuilder formatted = new StringBuilder();
		if(alarm.isUntilMode())
			formatted.append(context.getResources().getString(R.string.until))
				.append(" ")
				.append(Utils.dateFormat4.format(alarm.getUntilDate().getTime()))
				.append(".")
				.append(" ");
		else if(alarm.getRepeatCount() != 0)
			formatted.append(alarm.getRepeatCount())
				.append(" ")
				.append(context.getResources().getQuantityString(R.plurals.times, alarm.getRepeatCount()))
				.append(" ");
		if(alarm.isSelectedMode()) {
			boolean wholeWeek = true;
			StringBuilder weekdays = new StringBuilder();
			for(int i = 0; i < 7; i++) {
				if(alarm.getSelectedDays()[i])
					weekdays.append(context.getResources().getString(context.getResources().getIdentifier("weekday" + i, "string", context.getPackageName())))
							.append(", ");
				else wholeWeek = false;
			}
			weekdays.delete(weekdays.length() - 2, weekdays.length());
			formatted.append(wholeWeek ? context.getResources().getString(R.string.everyday) : weekdays.toString()).append(".");
		} else {
			int periodId = context.getResources().getIdentifier("everyPeriod" + alarm.getEveryPeriod(), "plurals", context.getPackageName());
			formatted.append(context.getResources().getString(R.string.every))
					.append(" ")
					.append(alarm.getEveryAmount() == 1 ? "" : alarm.getEveryAmount() + " ")
					.append(context.getResources().getQuantityString(periodId, alarm.getEveryAmount()))
					.append(".");
		}
		return formatted.toString();
	}
}
