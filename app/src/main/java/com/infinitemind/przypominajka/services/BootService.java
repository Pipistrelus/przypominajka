package com.infinitemind.przypominajka.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.infinitemind.przypominajka.alarms.Alarms;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.notifications.Notifications;

import java.util.Objects;

public class BootService extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if(Objects.equals(intent.getAction(), Intent.ACTION_BOOT_COMPLETED)) {
			Data.loadNotes(context);
			Data.loadCategories(context);
			Data.loadSettings(context);
			for(Note note : Data.allNotes)
				if(!note.isChecked() && note.isAlarmSet() && note.getAlarm() != null)
					Alarms.setAlarm(context, note, true);

			if(Data.quickAddEnabled)
				Notifications.showQuickAddNotification(context);
		}
	}
}
