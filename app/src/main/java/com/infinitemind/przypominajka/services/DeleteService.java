package com.infinitemind.przypominajka.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.notifications.Notifications;

import java.util.UUID;

public class DeleteService extends IntentService {

	public DeleteService() {
		super("DeleteService");
	}

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		Data.loadNotes(getApplicationContext());
		if(intent != null) {
			String stringID = intent.getStringExtra("id");
			if(stringID != null && !stringID.isEmpty()) {
				Note note = Data.getItemById(Note.class, UUID.fromString(stringID));
				if(note != null)
					Notifications.showNotification(getApplicationContext(), note, true);
			}
		}
	}
}
