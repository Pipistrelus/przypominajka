package com.infinitemind.przypominajka.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.infinitemind.przypominajka.fragments.LockAddonFragment;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.notifications.Notifications;

import java.util.UUID;

public class UnlockService extends IntentService {

	public UnlockService() {
		super("UnlockService");
	}

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		Data.loadNotes(getApplicationContext());
		if(intent != null) {
			String stringID = intent.getStringExtra("id");
			if(stringID != null && !stringID.isEmpty()) {
				Note note = Data.getItemById(Note.class, UUID.fromString(stringID));
				if(note != null) {
					note.setLockState(LockAddonFragment.UNLOCKED);
					Data.saveNotes(getApplicationContext());
					Notifications.showNotification(getApplicationContext(), note, true);
				}
			}
		}
	}
}
