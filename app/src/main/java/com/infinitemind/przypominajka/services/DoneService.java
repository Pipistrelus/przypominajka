package com.infinitemind.przypominajka.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.infinitemind.przypominajka.alarms.Alarms;
import com.infinitemind.przypominajka.models.Data;
import com.infinitemind.przypominajka.models.Note;
import com.infinitemind.przypominajka.notifications.Notifications;

import java.util.UUID;

public class DoneService extends IntentService {

	public DoneService() {
		super("DoneService");
	}

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		Data.loadNotes(getApplicationContext());
		if(intent != null) {
			String stringID = intent.getStringExtra("id");
			if(stringID != null && !stringID.isEmpty()) {
				Note note = Data.getItemById(Note.class, UUID.fromString(stringID));
				if(note != null) {
					Alarms.cancelAlarm(getApplicationContext(), note);
					Notifications.hideNotification(getApplicationContext(), note.getId().hashCode());
					note.setChecked(true);
					Data.saveNotes(getApplicationContext());
				}
			}
		}
	}
}
